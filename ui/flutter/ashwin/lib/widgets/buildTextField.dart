import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class BuildTextField extends StatelessWidget {
  final String label;
  final double spendingAmount;
  final double spendingPctOfTotal;

  BuildTextField(this.label, this.spendingAmount, this.spendingPctOfTotal);

  @override
  Widget build(BuildContext context) {
    Widget _buildTextField(
        String label, TextEditingController controller, bool isPassword){
      return Theme(
        data: Theme.of(context).copyWith(primaryColor: Colors.blue),
        child: TextField(
          controller: controller,
          decoration: InputDecoration(
            labelText: label,
            border: OutlineInputBorder(),
          ),
        ),
      );
    }

  }
}