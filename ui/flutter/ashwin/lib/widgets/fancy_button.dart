import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FancyButton extends StatelessWidget{
  FancyButton(label, {@required this.onPressed});

  final GestureTapCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      fillColor: Colors.blue,
      splashColor: Colors.lightBlue,
      child: Padding(
        padding: const EdgeInsets.symmetric(
            vertical: 8.0,
            horizontal: 20.0,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: const <Widget>[
            RotatedBox(
              quarterTurns: 1,
              child: Icon(
                Icons.explore,
                color: Colors.amber,
              ),
            ),
            SizedBox(width: 10.0,),
            Text(
              "LOGIN WITH GOOGLE",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
      onPressed: onPressed,
      shape: const StadiumBorder(),
    );
    // TODO: implement build
    throw UnimplementedError();
  }
}