

import 'package:ashwin/route.dart';
import 'package:ashwin/screens/splash_screen.dart';
import 'package:ashwin/screens/login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Doctor App',
      /*theme: new ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color(0xFF2196f3),
        accentColor: const Color(0xFF2196f3),
        canvasColor: const Color(0xFFfafafa),
      ),*/
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.amber,
        errorColor: Colors.red,
        fontFamily: 'Quicksand',
        textTheme: ThemeData.light().textTheme.copyWith(
          title: TextStyle(
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
          button: TextStyle(color: Colors.white),
        ),
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
            title: TextStyle(
              //fontFamily: 'Aladin',
              fontFamily: 'Montserrat',
              fontSize: 20,
              //fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,

      );
      //home: MyHomePage(title: 'Ashwin'),
  }
}


class FirstScreen extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
      appBar: new AppBar(title: new Text("Ashwin"),),
      /*body: new Checkbox(value: false,
          onChanged: (bool newValue){
            Navigator.push(ctxt,
              new MaterialPageRoute(builder: (ctxt) => new SecondScreen())
            );
          }),*/

        body: Center(
          child: Column(

            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const SizedBox(height: 30),
              RaisedButton(
                onPressed: () {Navigator.push(ctxt,
                    new MaterialPageRoute(builder: (ctxt) => new LoginScreen())
                );},
                textColor: Colors.white,
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: <Color>[
                        Color(0xFF0D47A1),
                        Color(0xFF1976D2),
                        Color(0xFF42A5F5),
                      ],
                    ),
                  ),
                  padding: const EdgeInsets.all(10.0),
                  child: const Text(
                      'DOCTOR APP',
                      style: TextStyle(fontSize: 20)
                  ),
                ),
              ),
              /*Text(
              'DOCTOR APP',
            ),*/
            ],
          ),
        ),
    );
  }
}



