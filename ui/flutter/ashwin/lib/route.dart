import 'package:ashwin/main.dart';
import 'package:ashwin/screens/splash_screen.dart';
import 'package:ashwin/screens/login_screen.dart';
import 'package:ashwin/screens/dashboard_screen.dart';
import 'package:ashwin/screens/waiting_room_screen.dart';
import 'package:ashwin/screens/add_new_appointment_screen.dart';
import 'package:ashwin/screens/patient_profile_screen.dart';

import 'package:ashwin/screens/patient/dashboard_screen.dart';
import 'package:ashwin/screens/patient/sign_up_screen.dart';
import 'package:ashwin/screens/patient/doctors_list_screen.dart';
import 'package:ashwin/screens/patient/doctor_profile_screen.dart';
import 'package:ashwin/screens/patient/book_appointment_screen.dart';
import 'package:ashwin/screens/patient/appointment_confirmation_screen.dart';
import 'package:ashwin/screens/patient/my_profile_screen.dart';
import 'package:ashwin/screens/patient/waiting_room_screen.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings settings){
    // Getting arguments passed in while calling Navigator.pushName
    final args = settings.arguments;
    switch (settings.name){

      case '/':
        return MaterialPageRoute(builder:(_) => SplashScreen());
      case '/login':
        return MaterialPageRoute(builder:(_) => LoginScreen());

      // Doctor App
      case '/waiting_room':
        return MaterialPageRoute(builder:(_) => WaitingRoomScreen());
      case '/patient_profile':
        return MaterialPageRoute(builder:(_) => PatientProfileScreen());
      case '/add_new_appointment':
        return MaterialPageRoute(builder:(_) => AddNewAppointmentScreen());
      /*case '/appointment_confirmation':
        return MaterialPageRoute(builder:(_) => PatientProfileScreen());*/

      // Patient App
      case '/patient/sign_up':
        return MaterialPageRoute(builder:(_) => PatientSignUpScreen());
      case '/patient/dashboard':
        return MaterialPageRoute(builder:(_) => PatientDashboardScreen());
      case '/patient/my_profile':
        return MaterialPageRoute(builder:(_) => MyProfileScreen());
      case '/patient/doctors_list':
        return MaterialPageRoute(builder:(_) => DoctorsListScreen());
      case '/patient/doctor_profile':
        return MaterialPageRoute(builder:(_) => DoctorProfileScreen());
      case '/patient/book_appointment':
        return MaterialPageRoute(builder:(_) => BookAppointmentScreen());
      case '/patient/appointment_confirmation':
        return MaterialPageRoute(builder:(_) => AppointmentConfirmationScreen());
      case '/patient/waiting_room_info':
        return MaterialPageRoute(builder:(_) => PatientWaitingRoomScreen());



      case '/dashboard':
        if(args is String){
          return MaterialPageRoute(
            builder:(_) => DashboardScreen()
          );
        }

        return _errorRoute();
      default:
        return _errorRoute();

    }
  }

  static Route<dynamic> _errorRoute(){
    return MaterialPageRoute(builder: (_){
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        )
      );
    });
  }
}