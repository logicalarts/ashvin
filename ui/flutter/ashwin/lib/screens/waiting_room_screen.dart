import 'dart:async';

import 'package:flutter/material.dart';

class WaitingRoomScreen extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
      appBar: new AppBar(title: new Text("Waiting Room"),),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album),
            title: Text('Mangesh Sawant'),
          ),
          ListTile(
            leading: Icon(Icons.photo_album),
            title: Text('Anil Jadhav'),
          ),
          ListTile(
            leading: Icon(Icons.photo_album),
            title: Text('Suresh Patil'),
          ),
        ],
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(ctxt).pushNamed('/add_new_appointment');
        },
        tooltip: 'New Appointment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.

    );
  }
}