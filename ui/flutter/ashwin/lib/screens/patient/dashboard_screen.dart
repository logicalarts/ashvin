import 'package:ashwin/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PatientDashboardScreen extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
        appBar: new AppBar(title: new Text("Patient Dashboard"),),
        body: Center(
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(10.0),
                color: Colors.blueGrey[100],
                width: 400.0,
                height: 150.0,
                child: FlatButton(
                  onPressed: () {
                    //Navigator.push(ctxt, new MaterialPageRoute(builder: (ctxt) => new PatientProfileScreen()));
                    Navigator.of(ctxt).pushNamed('/patient/my_profile');
                  },
                  child: new Text("My Profile"),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(10.0),
                color: Colors.blueGrey[100],
                width: 400.0,
                height: 150.0,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(ctxt).pushNamed('/patient/doctors_list');
                  },
                  child: new Text("Doctors List"),
                ),
              ),Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(10.0),
                color: Colors.blueGrey[100],
                width: 400.0,
                height: 150.0,
                child: FlatButton(
                  onPressed: () {
                    Navigator.of(ctxt).pushNamed('/patient/waiting_room_info');
                  },
                  child: new Text("Waiting Room"),
                ),
              ),
            ],
          ),
        )


    );
  }
}