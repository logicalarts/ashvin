import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PatientWaitingRoomScreen extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
      appBar: new AppBar(title: new Text("Waiting Room"),),
      body: Center(
        child: Container(
          constraints: BoxConstraints.expand(),
          //padding: EdgeInsets.all(0.0),
          //color: Color.fromARGB(100, 60, 165, 245),
          //alignment: AlignmentDirectional(0.0, 0.0),
          child: Container(
            padding: new EdgeInsets.all(40.0),
            color: Color.fromARGB(100, 60, 165, 245),
            child: Column(
              children: <Widget>[
                SizedBox(height: 20.0,),
                Text(
                  "5",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 80,
                    ),
                ),
                SizedBox(height: 20.0,),
                Text("patient before you."),
                SizedBox(height: 20.0,),
                Text("It will take around"),
                SizedBox(height: 20.0,),
                Text(
                  "5:40",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 80,
                  ),
                ),
                SizedBox(height: 20.0,),
                Text("min"),
                SizedBox(height: 20.0,),
                Text("please wait for your turn."),
              ],
            ),
          ),

        ),

      ),


    );
  }
}