import 'package:ashwin/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PatientSignUpScreen extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
      appBar: new AppBar(title: new Text("Sign Up"),),
      body: Container(
        padding: const EdgeInsets.all(30.0),
        child: Center(

          child: Column(

            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              //const SizedBox(height: 30),
              TextField(
                decoration: InputDecoration(
                  labelText: "FirstName",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20.0,),
              TextField(
                decoration: InputDecoration(
                  labelText: "Last Name",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20.0,),
              TextField(
                decoration: InputDecoration(
                  labelText: "Phone no",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20.0,),
              TextField(
                decoration: InputDecoration(
                  labelText: "Gender",
                  border: OutlineInputBorder(),
                ),
              ),
              SizedBox(height: 20.0,),
              TextField(
                decoration: InputDecoration(
                  labelText: "BirthDate",
                  border: OutlineInputBorder(),
                ),
              ),

              SizedBox(height: 20.0,),
              Container(
                width: 300,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.of(ctxt).pushNamed('/patient/dashboard');
                  },
                  textColor: Colors.white,
                  splashColor: Colors.lightBlue,
                  padding: const EdgeInsets.all(0.0),
                  child: Container(
                    width: double.infinity,
                    height: 45,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        colors: <Color>[
                          Color(0xFF0D47A1),
                          Color(0xFF1976D2),
                          Color(0xFF42A5F5),
                        ],
                      ),
                    ),
                    padding: const EdgeInsets.all(10.0),
                    alignment: Alignment.center,
                    child: const Text(
                        'SIGN UP',
                        style: TextStyle(fontWeight: FontWeight.bold)
                    ),
                  ),
                ),
              ),


            ],
          ),
        ),
      )
    );
  }
}