import 'package:ashwin/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DoctorsListScreen extends StatelessWidget {
        @override
        Widget build (BuildContext ctxt){
      return new Scaffold(
        appBar: new AppBar(title: new Text("Doctor List"),),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Dr. Sachin Pathak'),
              onLongPress: (){
                Navigator.of(ctxt).pushNamed('/patient/doctor_profile');
              },
            ),
            ListTile(
              leading: Icon(Icons.photo_album),
              title: Text('Dr. Neelam Wagh'),
            ),
          ],
        ),

        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.of(ctxt).pushNamed('/patient/book_appointment');
          },
          tooltip: 'New Appointment',
          child: Icon(Icons.add),
        ),

    );
  }
}