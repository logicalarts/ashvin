import 'dart:async';

import 'package:flutter/material.dart';
import 'package:ashwin/widgets/fancy_button.dart';
//import './main.dart';


class LoginScreen extends StatelessWidget {
  //final GestureTapCallback onPressed;
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
        appBar: new AppBar(title: new Text("Login"),),
        body: Container(
          padding: const EdgeInsets.all(30.0),
          child: Center(

            child: Column(

              //mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //const SizedBox(height: 30),
                SizedBox(height: 40.0,),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Username",
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 20.0,),
                TextField(
                  decoration: InputDecoration(
                    labelText: "Password",
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 8.0,),
                Container(
                  alignment: Alignment.bottomRight,
                  child: Text(
                    'forgot password',
                  ),
                ),
                SizedBox(height: 20.0,),
                Container(
                  width: 300,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(ctxt).pushNamed('/dashboard',arguments: 'Hello there from the first page!');
                    },
                    textColor: Colors.white,
                    splashColor: Colors.lightBlue,
                    padding: const EdgeInsets.all(0.0),
                    child: Container(
                      width: double.infinity,
                      height: 45,
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xFF0D47A1),
                            Color(0xFF1976D2),
                            Color(0xFF42A5F5),
                          ],
                        ),
                      ),
                      padding: const EdgeInsets.all(10.0),
                      alignment: Alignment.center,
                      child: const Text(
                          'LOGIN',
                          style: TextStyle(fontWeight: FontWeight.bold)
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 20.0,),
                Container(
                  child: Text(
                    '.................................. or ..................................',
                  ),
                ),
                SizedBox(height: 20.0,),
                Container(
                  width: 300,
                  height: 50,
                  child: FancyButton("Login",
                    onPressed: (){
                      Navigator.of(ctxt).pushNamed('/patient/dashboard',arguments: 'Hello there from the first page!');
                    },
                  ),
                ),
                SizedBox(height: 8.0,),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(ctxt).pushNamed('/patient/sign_up',arguments: 'Signup here!');
                    },
                    child: new Padding(
                      padding: new EdgeInsets.all(10.0),
                      child: new Text("New to Ashwin? Signup here"),
                    ),
                  ),

                ),

              ],
            ),
          ),
        )



    );


  }


}