import 'dart:async';

import 'package:flutter/material.dart';



class DashboardScreen extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt){
    return new Scaffold(
        appBar: new AppBar(title: new Text("Dashboard"),),
        body: Center(
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(10.0),
                color: Colors.blueGrey[100],
                width: 400.0,
                height: 150.0,
                child: FlatButton(
                  onPressed: () {
                    //Navigator.push(ctxt,  new MaterialPageRoute(builder: (ctxt) => new WaitingRoomScreen()));
                  },
                  child: new Text("Stastictics"),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(10.0),
                color: Colors.blueGrey[100],
                width: 400.0,
                height: 150.0,
                child: FlatButton(
                  onPressed: () {
                    //Navigator.push(ctxt, new MaterialPageRoute(builder: (ctxt) => new WaitingRoomScreen()));
                    Navigator.of(ctxt).pushNamed('/waiting_room');
                  },
                  child: new Text("Waiting Room"),
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.all(10.0),
                color: Colors.blueGrey[100],
                width: 400.0,
                height: 150.0,
                child: FlatButton(
                  /*onPressed: () {
                  Navigator.pushNamed(ctxt, "YourRoute");
                },*/
                  onPressed: () {
                    //Navigator.push(ctxt, new MaterialPageRoute(builder: (ctxt) => new PatientProfileScreen()));
                    Navigator.of(ctxt).pushNamed('/patient_profile');
                  },
                  child: new Text("Current Patient"),
                ),
              ),
            ],
          ),
        )


    );
  }
}

