
import 'package:flutter/material.dart';

import './widgets/splash_screen.dart';
import './widgets/login_screen.dart';
import './widgets/receiptionist_dashboard_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ashvin',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Colors.amber,
        errorColor: Colors.red,
        fontFamily: 'Quicksand',
        textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                fontFamily: 'Montserrat',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              button: TextStyle(color: Colors.white),
            ),
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(
                  fontFamily: 'Aladin',
                  fontSize: 40,
                  //fontWeight: FontWeight.bold,
                ),
              ),
        ),
      ),
      //home: MyHomePage(),
      //home: LoginScreen(),
      home: SplashScreen(),
      routes: {
        LoginScreen.routeName: (ctx) => LoginScreen(),
        MyHomePage.routeName: (ctx) => MyHomePage(),
      }
    );
  }
}

/*class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Patient> _patientAppointments = [
    // Patient(
    //   id: 'p1',
    //   firstName: 'Roshan',
    //   lastName: 'Sanap',
    //   birthDate: DateTime.now(),
    // ),
    // Patient(
    //   id: 'p2',
    //   firstName: 'Sandip',
    //   lastName: 'Shah',
    //   birthDate: DateTime.now(),
    // ),
  ];

  List<Patient> get _recentAppointments {
    return _patientAppointments.where((tx) {
      return tx.birthDate.isAfter(
        DateTime.now().subtract(
          Duration(days: 7),
        ),
      );
    }).toList();
  }

  void _addNewAppointment(String firstName, String lastName, DateTime birthDate) {
    final newTx = Patient(
      firstName: firstName,
      lastName: lastName,
      birthDate: birthDate,
      id: DateTime.now().toString(),
    );

    setState(() {
      _patientAppointments.add(newTx);
    });
  }

  void _startAddNewAppointment(BuildContext ctx) {
    showModalBottomSheet(
      context: ctx,
      builder: (_) {
        return GestureDetector(
          onTap: () {},
          child: NewAppointment(_addNewAppointment),
          behavior: HitTestBehavior.opaque,
        );
      },
    );
  }

  void _deleteAppointment(String id) {
    setState(() {
      _patientAppointments.removeWhere((tx) => tx.id == id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    final appBar = AppBar(
      title: Text('Ashvin'),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.add),
          onPressed: () => _startAddNewAppointment(context),
        ),
      ],
    );

    return Scaffold(
      appBar: appBar,
      body: SingleChildScrollView(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height - mediaQuery.padding.top ) *
                  0.3,
              child: Chart(_recentAppointments),
            ),
            Container(
              height: (mediaQuery.size.height -
                      appBar.preferredSize.height - mediaQuery.padding.top) *
                  0.7,
              child: AppointmentQueue(_patientAppointments, _deleteAppointment),
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Platform.isIOS ? Container() : FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _startAddNewAppointment(context),
      ),
    );
  }
}
*/