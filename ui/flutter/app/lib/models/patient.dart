import 'package:flutter/foundation.dart';

class Patient {
  String id;
  String firstName;
  String lastName;
  DateTime birthDate;

  Patient({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.birthDate,
  });
}
