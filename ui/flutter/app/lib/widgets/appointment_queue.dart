import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../models/patient.dart';

class AppointmentQueue extends StatelessWidget {
  final List<Patient> transactions;
  final Function deleteTx;

  AppointmentQueue(this.transactions, this.deleteTx);

  @override
  Widget build(BuildContext context) {
    return transactions.isEmpty
        ? Column(
            children: <Widget>[
              Text(
                'No patient in queue!',
                style: Theme.of(context).textTheme.title,
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                height: 200,
                child:
                    Image.asset('assets/images/waiting.png', fit: BoxFit.cover),
              ),
            ],
          )
        : ListView.builder(
            itemBuilder: (ctx, index) {
              return Card(
                elevation: 5,
                margin: EdgeInsets.symmetric(
                  vertical: 8,
                  horizontal: 5,
                ),
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 40,
                    child: Padding(
                      padding: EdgeInsets.all(6),
                      child: FittedBox(
                        // child: Text('\$${transactions[index].title}'),
                        child: Image.asset(
                          'assets/images/profile-placeholder.png',
                        ),
                        // fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  title: Text(
                    "${transactions[index].firstName} ${transactions[index].lastName}",
                    style: Theme.of(context).textTheme.title,
                  ),
                  subtitle: Text(
                    "Last visited on ${DateFormat.yMMMd().format(transactions[index].birthDate)}",
                  ),
                  trailing: MediaQuery.of(context).size.width > 460
                      ? FlatButton.icon(
                          icon: Icon(Icons.delete),
                          label: Text('Delete'),
                          textColor: Theme.of(context).errorColor,
                          onPressed: () => deleteTx(transactions[index].id),
                        )
                      : IconButton(
                          icon: Icon(Icons.edit),
                          color: Theme.of(context).errorColor,
                          onPressed: () => deleteTx(transactions[index].id),
                        ),
                ),
              );
            },
            itemCount: transactions.length,
          );
  }
}
