import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import './char_bar.dart';
import '../models/patient.dart';

class Chart extends StatelessWidget {
  final List<Patient> recentTransacitons;

  Chart(this.recentTransacitons);

  List<Map<String, Object>> get groupedTransactionValues {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(
        Duration(days: index),
      );
      var totalSum = 0.0;

      for (var i = 0; i < recentTransacitons.length; i++) {
        if (recentTransacitons[i].birthDate.day == weekDay.day &&
            recentTransacitons[i].birthDate.month == weekDay.month &&
            recentTransacitons[i].birthDate.year == weekDay.year) {
            //totalSum += recentTransacitons[i].amount;
            totalSum += 1;
        }
      }

      // print(DateFormat.E().format(weekDay));
      // print(totalSum);

      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum,
      };
    }).reversed.toList();
  }

  double get totalSpending {
    return groupedTransactionValues.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    // print(groupedTransactionValues);
    return Card(
        elevation: 6,
        margin: EdgeInsets.all(20),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: groupedTransactionValues.map((data) {
              return Flexible(
                fit: FlexFit.tight,
                child: ChartBar(
                  data['day'],
                  data['amount'],
                  totalSpending == 0.0
                      ? 0.0
                      : (data['amount'] as double) / totalSpending,
                ),
              );
            }).toList(),
          ),
      ),
    );
  }
}
