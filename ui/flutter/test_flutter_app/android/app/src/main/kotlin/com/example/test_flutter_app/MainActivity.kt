package com.example.test_flutter_app

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

  var authzData:String = ""

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)
    authzData = (intent.dataString as Nothing?).toString()
      println("Intent action" + intent.action)
      println("Intent data" + intent.data)
      println("Intent type" + intent.type)
      MethodChannel(flutterView, "app.channel.auth.data")
              .setMethodCallHandler { methodCall, result ->
                  result.success(getAccessToken())
              }
  }

  fun getAccessToken(): String {
    return this.authzData
  }

  override fun onResume() {
    super.onResume()
    //authzData = intent.dataString as Nothing?
    println("Intent action" + intent.action)
    println("Intent data" + intent.data)
    println("Intent type" + intent.type)
  }

  override fun onNewIntent(intent: Intent?) {
    val uri = intent?.data
    val uriParts = uri.toString().split("#")
    authzData = uriParts.get(1)
    println(uri)
  }
}
