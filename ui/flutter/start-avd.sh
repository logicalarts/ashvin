#!/bin/bash
set -x
pushd .;
cd ~/Android/Sdk/emulator || exit
# Start the emulator with writable syste image
./emulator -avd "Pixel_3_XL_API_30" -writable-system &
# Wait for the emulator to start
sleep 30
cd ~/Android/Sdk/emulator || exit
cd ..
cd platform-tools || exit
# Become root so that we can write files
./adb root
./adb remount
./adb shell
#cd system
#cd etc
#cat hosts
#echo "192.168.39.5 api.ashvin.com" >> hosts
#echo "192.168.39.5 ui.ashvin.com" >> hosts
#echo "192.168.39.5 login.ashvin.com" >> hosts
#echo "192.168.39.5 db.ashvin.com" >> hosts

# Copy /etc/hosts file so that domains login.ashvin.com will be resolved to minikube k8s host
./adb push /etc/hosts /system/etc/hosts
popd || exit;
