FROM node:10-alpine
# Create app directory
WORKDIR /usr/src/app
COPY . .
RUN npm install
RUN chmod 777 start-server.sh
EXPOSE 3000
CMD [ "/bin/ash","start-server.sh" ]
