import React, { useEffect, useState } from 'react'
import * as Globals from "./globals"
import PatientCard from './patientCard'

interface IQueueEntry {
    id: string
    patientId: string
    appointmentId: string
    index: number
    workingDay: Date
}

function Queue() {

    const [queueItems, setQueueItems] = useState<IQueueEntry[]>([])

    const fetchQueueEntries = () => {
        const accessToken = Globals.getToken()
        const tokenObject = Globals.getParsedTokenObject()
        const workingDay = Globals.formatDateForAPI(new Date())
        if (accessToken !== null) {
            fetch("http://api.ashvin.com/queues/day/" + workingDay, {
                headers: { "Authorization": "Bearer " + accessToken, "X-TENANT-ID": tokenObject.tenant_authorities[0].id },
                method: "GET",
            })
                .then(response => {
                    const resultQueueEntries: IQueueEntry[] = []
                    if (response.status === 200) {
                        response.json().then(data => {
                            const results: any[] = data
                            if (results.length > 0) {
                                for (const result of results) {
                                    const queueEntry: IQueueEntry = {
                                        appointmentId: result.appointmentId, id: result.id, index: result.index, patientId: result.patientId,
                                        workingDay: result.workingDay
                                    }
                                    resultQueueEntries.push(queueEntry)
                                }
                            }
                            setQueueItems(resultQueueEntries)
                        })
                    }
                    else {
                        setQueueItems(resultQueueEntries)
                    }
                })
                .catch(ex => alert(ex))
        }
    }

    useEffect(() => {      
        fetchQueueEntries()
    }, [])

    return (
        <div>
            {queueItems.map((queueEntry, index) => {
                return (
                    <PatientCard id={queueEntry.patientId} key={index}/>
                )
            })}
        </div>
    )

}

export default Queue