import * as React from "react"
import * as Constants from './globals'

class Profile extends React.Component {

    constructor(props: any){
        super(props)
    }

    public render() {
        const tokenObject = Constants.getParsedTokenObject()
        let userName = ""
        let tenantName = ""
        if(tokenObject !== null) {
            if(tokenObject !== null) {
                userName = tokenObject.user_name !== null ? tokenObject.user_name : null
                tenantName = tokenObject.tenant_authorities !== null ? tokenObject.tenant_authorities[0].name : null
            }
        }
        const isLoggedIn = (tokenObject !== null) 
        return (
            <div className="profile">
                <table className="profileTable">
                    <tbody>
                        {isLoggedIn ? ( 
                        <tr>
                            <td align="left"> Welcome, {userName} @ {tenantName} </td>
                            <td align="right"> <a href="../logout">Logout</a></td>
                        </tr>
                        ) :
                        (
                        <tr>
                            <td> <a href={Constants.AUTHZ_URL}>Login</a></td>
                        </tr>
                        )}
                    </tbody>
                </table>
            </div>
        )
    }

}

export default Profile