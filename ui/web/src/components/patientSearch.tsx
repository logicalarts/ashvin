import * as React from "react"
import { FaPlus, FaSearch, FaUserPlus } from 'react-icons/fa'
// import { NavLink } from 'react-router-dom'
import { withRouter } from 'react-router-dom'
import { isNullOrUndefined } from 'util'
import * as Globals from "./globals"

interface IPatient {
    id: string;
    firstName: string;
    lastName: string;
    dateOfBirth: Date;
}

interface IPatientSearchState {
    matchingResults: IPatient[]
}

class PatientSearch extends React.Component<any, IPatientSearchState> {

    public props: any
    private lastName: any
    private dateOfBirth: any

    constructor(props: any, state: IPatientSearchState) {
        super(props, state)
        this.props = props
        this.state = { matchingResults: [] }
        this.searchPatients = this.searchPatients.bind(this)
        this.renderSearchResults = this.renderSearchResults.bind(this)
        this.renderNewPatient = this.renderNewPatient.bind(this)
        this.navigateToNewAppointment = this.navigateToNewAppointment.bind(this)
        this.navigateToNewPatient = this.navigateToNewPatient.bind(this)
    }

    public searchPatients() {
        const accessToken = Globals.getToken()
        const tokenObject = Globals.getParsedTokenObject()
        const lastName = this.lastName.value
        const dob = new Date(this.dateOfBirth.value)
        const dateOfBirth = dob.getFullYear() + "-" + (dob.getMonth() + 1) + "-" + dob.getDate()
        if (accessToken !== null) {
            fetch("http://api.ashvin.com/patients/search?lastName=" + lastName + "&dateOfBirth=" + dateOfBirth, {
                headers: { "Authorization": "Bearer " + accessToken, "X-TENANT-ID": tokenObject.tenant_authorities[0].id },
                method: "GET",
            })
                .then(response => {
                    const resultPatients: IPatient[] = []
                    if (response.status === 200) {
                        response.json().then(data => {
                            const results: any[] = data
                            if (results.length > 0) {
                                for (const result of results) {
                                    const patient: IPatient = {
                                        dateOfBirth: result.dateOfBirth, firstName: result.firstName,
                                        id: result.id, lastName: result.lastName
                                    }
                                    resultPatients.push(patient)
                                }
                            }
                            this.setState({ matchingResults: resultPatients });
                        })
                    }
                    else {
                        this.setState({ matchingResults: resultPatients });
                    }
                })
                .catch(ex => alert(ex))
        }
    }

    public navigateToNewAppointment(e: any) {
        this.props.history.push({pathname: "/newAppointment", state:{ patientId: e.target.value}})
    }

    public navigateToNewPatient(e: any) {
        this.props.history.push({pathname: "/newPatient", state:{ lastName: this.lastName.value, dateOfBirth: this.dateOfBirth.value}})
    }

    public render() {
        const { matchingResults } = this.state
        return (
            <div className="patientSearch">
                <table>
                    <tbody>
                        <tr>
                            <td><label className="uk-form-label"><b>Last Name</b></label></td>
                            <td> : </td>
                            <td><input className="uk-input" type="text" defaultValue={"New"} ref={input => (this.lastName = input)} /></td>
                        </tr>
                        <tr>
                            <td><label className="uk-form-label"><b>Date of birth</b></label></td>
                            <td> : </td>
                            <td><input className="uk-input" type="text" defaultValue={"01/01/2001"} ref={input => (this.dateOfBirth = input)} /></td>
                        </tr>
                        <tr>
                            <td colSpan={3} align="center">
                                <button className="uk-button uk-button-primary" onClick={this.searchPatients}> <FaSearch/> Search </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr className="uk-divider-icon"/>
                <div className="patientSearchResults1">
                    {matchingResults.length > 0 ? this.renderSearchResults() : this.renderNewPatient()}
                </div>
            </div>
        )
    }

    public renderSearchResults() {
        const { matchingResults } = this.state
        return (
            matchingResults.map((patient, index) => {
                return (
                    <div className="uk-card uk-card-default uk-panel patientResult1" key={index}>
                        <div className="uk-card-body patientDetails1">
                        <button className="uk-button uk-button-primary uk-align-right" value={patient.id} onClick={this.navigateToNewAppointment}><FaPlus /></button>
                            <table>
                                <tbody>
                                    <tr>
                                        <td colSpan={2}><b> {patient.firstName} {patient.lastName} </b></td>
                                    </tr>
                                    <tr>
                                        <td><b>Date of Birth:</b> </td>
                                        <td> {Globals.formatDate(patient.dateOfBirth)} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                </div>
                )
            })
        )
    }

    public renderNewPatient() {
        const lastName = isNullOrUndefined(this.lastName) ? "" : this.lastName.value
        const dateOfBirth = isNullOrUndefined(this.dateOfBirth) ? "" : this.dateOfBirth.value
        if (lastName === "") {
            return (
                <div/>
            )
        }
        return (
            <div className="uk-card uk-card-default patientResult1">
                <div className="uk-card-body patientDetails1">
                <button className="uk-button uk-button-primary uk-align-right" onClick={this.navigateToNewPatient}><FaUserPlus /></button>
                    <table>
                        <tbody>
                            <tr>
                                <td><b>Last Name</b> </td>
                                <td> : {lastName} </td>
                            </tr>
                            <tr>
                                <td><b>Date of Birth:</b> </td>
                                <td> {Globals.formatDate(dateOfBirth)} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

}

export default withRouter(PatientSearch)