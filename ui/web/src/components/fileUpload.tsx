import * as jwt from "jsonwebtoken"
import * as React from "react"
import { FaUpload } from 'react-icons/fa'
import { AUTH_TOKEN_KEY } from './globals'

class FileUpload extends React.Component {

    private fileSelector: any
    private objectType: any
    
    constructor(props: any){
        super(props)
        this.uploadFile = this.uploadFile.bind(this)
    }

    public uploadFile() {
        const data = new FormData()
        const accessToken = localStorage.getItem(AUTH_TOKEN_KEY)
        if(accessToken !== null) {
            const tokenObject: any = jwt.decode(accessToken)
            data.append("file", this.fileSelector.files[0])
            data.append("objectType", this.objectType.value)
            fetch("http://api.ashvin.com/upload", {
                body: data,
                headers: { "Authorization" : "Bearer " + localStorage.getItem(AUTH_TOKEN_KEY) , "X-TENANT-ID": tokenObject.tenant_authorities[0].id},
                method: "POST",
            })
            .then(response => response.status !== 200 ? alert("Failed to upload file") : alert("File uploaded successfully!"))
            .catch(ex => alert(ex))
        }
    }

    public render() {
        const uploadFile = this.uploadFile
        return (
            <div className="file-upload">
                <table>
                    <tbody>
                        <tr>
                            <td> Object Type </td>
                            <td> : </td>
                            <td><input type="text" ref={input => (this.objectType = input)}/></td>
                        </tr>
                        <tr>
                            <td>File</td>
                            <td> : </td>
                            <td><input type="file" ref={input => (this.fileSelector = input)}/></td>
                        </tr>
                        <tr>
                            <td colSpan={3} align="right"> <button onClick={uploadFile}> <FaUpload/> Upload </button> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }

}

export default FileUpload