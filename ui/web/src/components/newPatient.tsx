import React from 'react'
import { FaUserPlus } from 'react-icons/fa'
import { withRouter } from 'react-router-dom'
import useInput from '../hooks/useInput'
import * as Globals from "./globals"

function NewPatient(props: any) {
    const { bind:bindEmail, value: email, reset:resetEmail } = useInput("")
    const { bind:bindFirstName, value: firstName, reset:resetFirstName } = useInput("")
    const { bind:bindLastName, value: lastName, reset:resetLastName } = useInput(props.lastName)
    const { bind:bindDateOfBirth, value: dateOfBirth, reset:resetDateOfBirth } = useInput(props.dateOfBirth)

    const createNewPatient =() => {
        const user = {email}
        const dobSplit = dateOfBirth.split("/")
        const formattedDate = dobSplit[2] + "-" + dobSplit[1] + "-" + dobSplit[0]
        const patient = {firstName, lastName, dateOfBirth: new Date(formattedDate)}
        const createPatientRequest = {user, patient}
        const accessToken = Globals.getToken()
        const tokenObject = Globals.getParsedTokenObject()
        if (accessToken !== null) {
            fetch("http://api.ashvin.com/patients", {
                body: JSON.stringify(createPatientRequest),
                headers: {
                    "Authorization": "Bearer " + accessToken, 
                    'Content-Type': 'application/json', 
                    "X-TENANT-ID": tokenObject.tenant_authorities[0].id 
                },
                method: "POST",
            })
                .then(response => {
                    if (response.status === 200) {
                        response.json().then(data => {
                            if (data) {
                                props.history.push({pathname: "/newAppointment", state: {patientId: data.id}})
                                resetEmail()
                                resetFirstName()
                                resetLastName()
                                resetDateOfBirth()
                            }
                        })
                    }
                })
                .catch(ex => alert(ex))
        }
    }

    return (
        <div>
            <table>
                <tbody>
                    <tr>
                        <td><label className="uk-form-label"><b>First Name:</b></label></td>
                        <td><input type="text" {...bindFirstName}/></td>
                    </tr>
                    <tr>
                        <td><label className="uk-form-label"><b>Last Name:</b></label></td>
                        <td><input type="text" {...bindLastName}/></td>
                    </tr>
                    <tr>
                        <td><label className="uk-form-label"><b>Date of Birth:</b></label></td>
                        <td><input type="text" {...bindDateOfBirth}/></td>
                    </tr>
                    <tr>
                        <td><label className="uk-form-label"><b>Email:</b></label></td>
                        <td><input type="text" {...bindEmail}/></td>
                    </tr>
                    <tr>
                        <td colSpan={2}>
                            <button className="uk-button uk-button-primary" onClick={createNewPatient}><FaUserPlus/> Create Patient</button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default withRouter(NewPatient)