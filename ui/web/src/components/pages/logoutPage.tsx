// import * as jwt from "jsonwebtoken"
import * as React from "react"
import * as Constants from '../globals'

class LogoutPage extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public componentDidMount() {
        document.location.href = Constants.TOKEN_REVOCATION_URL
        document.cookie.split(";").forEach(c => { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/")})
        localStorage.removeItem(Constants.AUTH_TOKEN_KEY)
        alert("You have logged out!");
        document.location.href = Constants.isDevEnvironment() ? "http://localhost:3000" : "http://ui.ashvin.com"
    }

    public render() {
        return (
            <div>
                You have logged out. <a href={Constants.AUTHZ_URL}>Login</a>
            </div>
                
        )
    }
}

export default LogoutPage