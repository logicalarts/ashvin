import * as React from "react"
import FileUpload from '../fileUpload';
import PageTemplate from '../pageTemplate'

class FileUploadPage extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public render() {
        return (
            <PageTemplate>
                <FileUpload />
            </PageTemplate>
        )
    }
}

export default FileUploadPage