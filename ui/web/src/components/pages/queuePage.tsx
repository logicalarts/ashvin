import React from 'react'
import PageTemplate from '../pageTemplate'
import Queue from '../queue'

function QueuePage() {
    return (
        <PageTemplate>
            <Queue />
        </PageTemplate>
    )
}

export default QueuePage