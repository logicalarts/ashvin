import React from 'react'
import NewAppointment from '../newAppointment'
import PageTemplate from '../pageTemplate'

function NewAppointmentPage(props: any) {
    const patientId = props.location.state.patientId
    return (
        <PageTemplate>
            <NewAppointment patientId={patientId} symptoms="" />
        </PageTemplate>
    )
}

export default NewAppointmentPage