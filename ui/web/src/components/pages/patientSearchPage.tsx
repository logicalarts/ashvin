import * as React from "react"
import PageTemplate from '../pageTemplate'
import PatientSearch from '../patientSearch'

class PatientSearchPage extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public render() {
        return (
            <PageTemplate>
                <PatientSearch />
            </PageTemplate>
        )
    }
}

export default PatientSearchPage