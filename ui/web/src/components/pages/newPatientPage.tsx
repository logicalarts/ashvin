import React from 'react'
import NewPatient from '../newPatient'
import PageTemplate from '../pageTemplate'

function NewPatientPage(props: any) {
    const lastName = props.location.state.lastName
    const dateOfBirth = props.location.state.dateOfBirth
    
    return (
        <PageTemplate>
            <NewPatient lastName={lastName} dateOfBirth={dateOfBirth} />
        </PageTemplate>
    )
}

export default NewPatientPage