import * as React from "react"
import { FaPeopleCarry, FaPersonBooth } from "react-icons/fa"
import { NavLink } from 'react-router-dom';
import PageTemplate from '../pageTemplate'

class HomePage extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public render() {
        return (
            <PageTemplate>
                <div className="uk-child-width-1-3@s uk-text-center">
                    <div>
                        <div className="uk-tile uk-tile-default uk-background-default uk-padding-small">
                            <p className="uk-h5">
                                <NavLink to="/patientSearch"> <FaPersonBooth/> Manage Patients</NavLink>
                            </p>
                        </div>
                    </div>
                    <div>
                        <div className="uk-tile uk-tile-default uk-background-default uk-padding-small">
                            <p className="uk-h5">
                                <NavLink to={{pathname: '/queue'}}> <FaPeopleCarry/> Current Patient Queue</NavLink>
                            </p>
                        </div>
                    </div>
                </div>
            </PageTemplate>
        )
    }
}

export default HomePage