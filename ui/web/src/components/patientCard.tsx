import React, { useEffect, useState } from 'react'
import * as Globals from "./globals"

interface IPatient {
    id: string
    firstName: string
    lastName: string
    dateOfBirth: string
}

function PatientCard(props: any) {

    const [patient, setPatient] = useState<IPatient>({ id: "", firstName: "", lastName: "", dateOfBirth: "" })
    const patientId = props.id

    const fetchPatient = () => {
        const accessToken = Globals.getToken()
        const tokenObject = Globals.getParsedTokenObject()
        if (accessToken !== null) {
            fetch("http://api.ashvin.com/patients/" + patientId, {
                headers: { "Authorization": "Bearer " + accessToken, "X-TENANT-ID": tokenObject.tenant_authorities[0].id },
                method: "GET",
            })
                .then(response => {
                    if (response.status === 200) {
                        response.json().then(data => {
                            if (data) {
                                setPatient(data)
                            }
                        })
                    }
                })
                .catch(ex => alert(ex))
        }
    }

    useEffect(() => {
        fetchPatient()
    }, []);

    return (
        <div className="uk-card uk-card-default patientResult1">
            <div className="uk-card-body patientDetails1">
                <table>
                    <tbody>
                        <tr>
                            <td colSpan={2}><b> {patient.firstName} {patient.lastName} </b></td>
                        </tr>
                        <tr>
                            <td><b>Date of Birth:</b> </td>
                            <td> {Globals.formatDate(patient.dateOfBirth)} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default PatientCard