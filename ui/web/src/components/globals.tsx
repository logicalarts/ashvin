import * as jwt from "jsonwebtoken"

export const isDevEnvironment = () => {
    return !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
}

export const AUTHZ_URL= isDevEnvironment() ? "http://login.ashvin.com/oauth/authorize?response_type=token&state=api&client_id=ui&scope=read%20write&redirect_uri=http%3A%2F%2Flocalhost:3000%2F%3F"
                        : "http://login.ashvin.com/oauth/authorize?response_type=token&state=api&client_id=ui&scope=read%20write&redirect_uri=http%3A%2F%2Fui.ashvin.com%2F%3F"
export const TOKEN_REVOCATION_URL="http://login.ashvin.com/logout"
export const AUTH_TOKEN_KEY = "AUTH_TOKEN"

export const getParsedTokenObject = () => {
    let tokenObject: any
    const accessToken = localStorage.getItem(AUTH_TOKEN_KEY)
    if(accessToken !== null) {
        tokenObject = jwt.decode(accessToken)
    }
    return tokenObject;
}

export const getToken = () => {
    return localStorage.getItem(AUTH_TOKEN_KEY);
}

export const formatDate = (d : any) => {
    const date = new Date(d)
    return date.getDate() +  " / " + (date.getMonth()+1) + " / " + date.getFullYear()
}

export const formatDateForAPI = (d : any) => {
    const date = new Date(d)
    return date.getFullYear() + "-" +  (date.getMonth()+1) + "-" + date.getDate()
}