import React from 'react'
import { FaPlus } from 'react-icons/fa'
import { withRouter } from 'react-router-dom'
import useInput from 'src/hooks/useInput'
import * as Globals from "./globals"
import PatientCard from './patientCard'

// interface INewAppointment {
//     patientId: string
//     symptoms: string
// }

function NewAppointment(props: any) {
    const patientId = props.patientId 
    const {bind: bindSymptoms, value:symptoms} = useInput(props.symptoms)

    const createNewAppointment = () => {
        const queueEntry = {patientId, workingDay: new Date() }
        const appointment = {patientId, symptoms, checkIn: new Date()}
        const queuePatientRequest = {queueEntry, appointment}
        const accessToken = Globals.getToken()
        const tokenObject = Globals.getParsedTokenObject()
        if (accessToken !== null) {
            fetch("http://api.ashvin.com/queues/patient", {
                body: JSON.stringify(queuePatientRequest),
                headers: {
                    "Authorization": "Bearer " + accessToken, 
                    'Content-Type': 'application/json', 
                    "X-TENANT-ID": tokenObject.tenant_authorities[0].id 
                },
                method: "POST",
            })
                .then(response => {
                    if (response.status === 200) {
                        response.json().then(data => {
                            if (data) {
                                props.history.push({pathname: "/queue"})
                            }
                        })
                    }
                })
                .catch(ex => alert(ex))
        }
    }

    return (
        <div>
            <PatientCard id={patientId}/>
            <section>
            <b>Symptoms:</b>
            <br/>
                <textarea rows={5} cols={30} {...bindSymptoms}/>
            </section>
            <section>
                <button className="uk-button uk-button-primary" onClick={createNewAppointment}> <FaPlus/> Add to Queue</button>
            </section>
        </div>
    )

}

export default withRouter(NewAppointment)