import * as React from "react"
import { MainMenu } from "./menus/menus"
import Profile from "./profile"

class PageTemplate extends React.Component {

    constructor(props: any) {
        super(props)
    }

    public render() {
        const children = this.props.children
        return (
          <div className="uk-section uk-section-default uk-preserve-color page1 page-container">
            <div className="uk-navbar-container  uk-navbar-transparent profile">
              <Profile/>
            </div>
            <div className="menu">
                <MainMenu />
            </div>
            <div className="uk-container ">
                {children}
            </div>
          </div>
        );
      }

}

export default PageTemplate
