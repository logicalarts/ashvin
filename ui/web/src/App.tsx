import * as QueryString from "query-string"
import * as React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import './App.css'
import * as Constants from './components/globals'
import HomePage from './components/pages/homePage'
import LogoutPage from './components/pages/logoutPage'
import NewAppointmentPage from './components/pages/newAppointmentPage'
import NewPatientPage from './components/pages/newPatientPage'
import PatientSearchPage from './components/pages/patientSearchPage'
import QueuePage from './components/pages/queuePage'

class App extends React.Component {

  public componentDidMount() {
    if(window.location.href.indexOf("access_token") > 0) {
      const parsedHash = QueryString.parse(location.hash)
      const accessToken = parsedHash.access_token == null ? "" : parsedHash.access_token + ""
      localStorage.setItem(Constants.AUTH_TOKEN_KEY, accessToken)
      // Navigate to root of the UI after successfull authentication
      document.location.href = "/"
    }
  }

  public render() {
    const AUTH_TOKEN = localStorage.getItem(Constants.AUTH_TOKEN_KEY)
    const isLoggedIn = AUTH_TOKEN !== null
    return (
        isLoggedIn ? (
          <Router>
            <div className="bg-img">
                <Switch>
                <Route path="/home" exact={true} component={HomePage} />
                <Route path="/patientSearch" exact={true} component={PatientSearchPage} />
                <Route path="/queue" exact={true} component={QueuePage} />
                <Route path="/newAppointment" exact={true} component={NewAppointmentPage} />
                <Route path="/newPatient" exact={true} component={NewPatientPage} />
                {/* <Route path="/upload" component={FileUploadPage} /> */}
                <Route path="/logout" exact={true} component={LogoutPage} />
                <Route path="/" exact={false} component={HomePage} />
              </Switch>
            </div>
          </Router>
        ) : (
        <div className="bg-img"> 
          <form className="container">
            Welcome to Ashvin!
            <br/>
            Please <a href={Constants.AUTHZ_URL}>Login</a>.
          </form>          
        </div>
        )
    )
  }
}

export default App
