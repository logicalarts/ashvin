import {useState} from 'react'

const useInput = (initialValue: any) => {
    const [value, setValue] = useState(initialValue)

    return {
        bind: {
            onChange: (event: any) => { setValue(event.target.value)},
            value,
        },
        reset:  () => setValue(''),
        setValue,
        value,
    }
}

export default useInput