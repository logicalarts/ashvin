package com.ashvin.api.resource

import com.ashvin.api.domain.CreatePatientRequest
import com.ashvin.platform.domain.Page
import com.ashvin.platform.domain.Patient
import com.ashvin.platform.domain.User

import javax.ws.rs.client.Entity
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType

import static com.ashvin.api.resource.Constants.HEADER_X_TENANT_ID

class PatientSpec extends BaseIntegrationSpec {

    def "Get patients by page succeeds"() {
        when: "Admin tries to get all patients"
        Page<Patient> pageRequest = Page.builder().size(10).number(0).build()
        def response =
                apiServer.path("/patients")
                        .queryParam("number", pageRequest.getNumber())
                        .queryParam("size", pageRequest.getSize())
                        .request(MediaType.APPLICATION_JSON)
                        .header(HEADER_X_TENANT_ID, "1")
                        .header("Authorization", "Bearer " + adminAccessToken)
                        .get()
        Page<Patient> patientPage = response.readEntity(new GenericType<Page<Patient>>() {})

        then: "API server returns patients by page"
        response != null
        response.status == 200
        patientPage != null
        patientPage.number == 0
        patientPage.size == 10
    }

    def "Creating new patient succeeds"() {
        when: "A new patient creation request is issued"
        def firstName = "Patient_FN"
        def lastName = "Patient_LN"
        def dob = new Date()
        Patient patient = Patient.builder()
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(dob).build()
        User user = User.builder().email("test_" + UUID.randomUUID()).build()
        CreatePatientRequest createPatientRequest = new CreatePatientRequest(user, patient)
        def response = apiServer.path("/patients")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(createPatientRequest, MediaType.APPLICATION_JSON))
        Patient createdPatient = response.readEntity(Patient.class)

        then: "New patient is created successfully"
        createdPatient.firstName == firstName
        createdPatient.lastName == lastName
        createdPatient.dateOfBirth == dob
    }

    def "Updating an existing patient succeeds"() {
        when: "An existing patient is updated"
        def firstName = "Patient_FN"
        def lastName = "Patient_LN"
        def dob = new Date()
        Patient patient = Patient.builder()
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(dob).build()
        User user = User.builder().email("test_" + UUID.randomUUID()).build()
        CreatePatientRequest createPatientRequest = new CreatePatientRequest(user, patient)
        def response = apiServer.path("/patients")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(createPatientRequest, MediaType.APPLICATION_JSON))
        Patient createdPatient = response.readEntity(Patient.class)
        def updatedDob = new Date();
        Patient updatedPatient = createdPatient.toBuilder()
                .firstName("Updated_" + firstName)
                .lastName("Updated_" + lastName).dateOfBirth(updatedDob)
                .build();
        def updateResponse = apiServer.path("/patients/${createdPatient.id}")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(updatedPatient, MediaType.APPLICATION_JSON))
        def savedPatient = updateResponse.readEntity(Patient.class)

        then: "Existing patient is updated successfully"
        savedPatient.firstName == "Updated_" + firstName
        savedPatient.lastName == "Updated_" + lastName
        savedPatient.dateOfBirth == updatedDob
    }

}
