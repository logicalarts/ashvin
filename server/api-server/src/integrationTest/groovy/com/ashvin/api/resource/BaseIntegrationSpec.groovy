package com.ashvin.api.resource

import com.ashvin.api.domain.CreatePatientRequest
import com.ashvin.platform.domain.Patient
import com.ashvin.platform.domain.User
import spock.lang.Shared
import spock.lang.Specification

import javax.ws.rs.client.Client
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.client.WebTarget
import javax.ws.rs.core.MediaType

class BaseIntegrationSpec extends Specification {

    static final String API_SERVER_URL = "http://api.ashvin.com"
    static final String AUTH_SERVER_URL = "http://login.ashvin.com"

    @Shared
    Client client = ClientBuilder.newClient();

    @Shared
    WebTarget apiServer

    @Shared
    WebTarget authServer

    @Shared
    def adminAccessToken

    @Shared
    String defaultTenantId = "1"

    def setupSpec() {
        apiServer = client.target(API_SERVER_URL)
        authServer = client.target(AUTH_SERVER_URL)
        adminAccessToken = obtainAccessToken("admin", "admin")
    }

    def obtainAccessToken(String username, String password) {
        def response =
                authServer.path("/oauth/token")
                        .queryParam("scope", "read write")
                        .queryParam("tenantId", "1")
                        .queryParam("grant_type", "password")
                        .queryParam("username", username)
                        .queryParam("password", password)
                        .request(MediaType.APPLICATION_JSON)
                        .header("X-TENANT-ID", "1")
                        .header("Authorization", "Basic dWk6YXNodmlu") //Encoded client id and client secret
                        .get()
        def accessToken = response.readEntity(Map.class).get("access_token")
        print "Access Token :" + accessToken
        return accessToken
    }
    
    Patient createPatient(String nameSuffix) {
        def firstName = "FN_" + nameSuffix
        def lastName = "LN_" + nameSuffix
        def dob = new Date()
        Patient patient = Patient.builder()
                .firstName(firstName)
                .lastName(lastName)
                .dateOfBirth(dob).build()
        User user = User.builder().email("test_" + UUID.randomUUID()).build()
        CreatePatientRequest createPatientRequest = new CreatePatientRequest(user, patient)
        def response = apiServer.path("/patients")
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(createPatientRequest, MediaType.APPLICATION_JSON))
        response.readEntity(Patient.class)
    }
}
