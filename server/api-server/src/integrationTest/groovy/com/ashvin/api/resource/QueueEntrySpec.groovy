package com.ashvin.api.resource

import com.ashvin.api.domain.QueuePatientRequest
import com.ashvin.platform.domain.Appointment
import com.ashvin.platform.domain.Patient
import com.ashvin.platform.domain.QueueEntry

import javax.ws.rs.client.Entity
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import java.text.SimpleDateFormat

import static com.ashvin.api.resource.Constants.HEADER_X_TENANT_ID

class QueueEntrySpec extends BaseIntegrationSpec {

    def "A new queue with patient appointment is created successfully"() {
        when: "A new patient is added to the queue"
        Patient patient = createPatient("NewQueue")
        def patientId = patient.id
        QueueEntry queueEntry = QueueEntry.builder().patientId(patientId).workingDay(workingDate()).build()
        Appointment appointment = Appointment.builder().patientId(patientId).checkIn(workingDate())
                .symptoms("Cough and cold").build()
        def queuePatientRequest = new QueuePatientRequest(queueEntry, appointment)
        def response =
                apiServer.path("/queues/patient")
                        .request(MediaType.APPLICATION_JSON)
                        .header(HEADER_X_TENANT_ID, defaultTenantId)
                        .header("Authorization", "Bearer " + adminAccessToken)
                        .post(Entity.entity(queuePatientRequest, MediaType.APPLICATION_JSON))
        List<QueueEntry> queueEntries = response.readEntity(new GenericType<List<QueueEntry>>() {})

        then: "Queue with new patient is returned"
        queueEntries != null
        queueEntries.size() > 0
    }

    def "Queue entries for a specific day are returned correctly"() {
        when: "Queue entries for a specific day are requested"
        def newQueueEntries = createQueueEntry("GetQueueByDay")
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
        def response =
                apiServer.path("/queues/day/" + simpleDateFormat.format(workingDate()))
                        .request(MediaType.APPLICATION_JSON)
                        .header(HEADER_X_TENANT_ID, defaultTenantId)
                        .header("Authorization", "Bearer " + adminAccessToken)
                        .get()
        List<QueueEntry> queueEntriesByDay = response.readEntity(new GenericType<List<QueueEntry>>() {})

        then: "Queue entries for specific day are returned"
        queueEntriesByDay != null
        queueEntriesByDay.size() > 0
        queueEntriesByDay.size() == newQueueEntries.size()
    }

    private List<QueueEntry> createQueueEntry(String patientName) {
        Patient patient = createPatient(patientName)
        def patientId = patient.id
        QueueEntry queueEntry = QueueEntry.builder().patientId(patientId).workingDay(workingDate()).build()
        Appointment appointment = Appointment.builder().patientId(patientId).checkIn(workingDate())
                .symptoms("Cough and cold").build()
        def queuePatientRequest = new QueuePatientRequest(queueEntry, appointment)
        def response =
                apiServer.path("/queues/patient")
                        .request(MediaType.APPLICATION_JSON)
                        .header(HEADER_X_TENANT_ID, defaultTenantId)
                        .header("Authorization", "Bearer " + adminAccessToken)
                        .post(Entity.entity(queuePatientRequest, MediaType.APPLICATION_JSON))
        response.readEntity(new GenericType<List<QueueEntry>>() {})
    }
    
    private static Date workingDate() {
        // Hack to make tests work till we switch to ZonedDateTime
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd")
        simpleDateFormat.parse(simpleDateFormat.format(new Date()))
    }

}
