package com.ashvin.api.resource;

import com.ashvin.api.domain.CreatePatientRequest;
import com.ashvin.platform.domain.Page;
import com.ashvin.platform.domain.Patient;
import com.ashvin.platform.domain.User;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.ashvin.platform.repo.PatientRepo;
import com.ashvin.platform.repo.UserRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;

@Path("/patients")
public class PatientResource {

    private Logger logger = LoggerFactory.getLogger(PatientResource.class);
    private PatientRepo patientRepo;
    private UserRepo userRepo;
    private MetricsProvider metricsProvider;
    private Tracer tracer;

    @Inject
    public PatientResource(DataSource dataSource, MetricsProvider metricsProvider, Tracer tracer) {
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
        this.patientRepo = new PatientRepo(dataSource, tracer);
        this.userRepo = new UserRepo(dataSource, tracer);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Patient> getPatients(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                               @BeanParam Page<Patient> pageRequest) throws SQLException {
        logger.info("--> Fetching all patients");
        return patientRepo.getAll(null, pageRequest);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Patient createPatient(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                 CreatePatientRequest createPatientRequest) throws SQLException {
        Patient patient = createPatientRequest.getPatient();
        User requestedUser = createPatientRequest.getUser();
        String email = requestedUser.getEmail();
        if(email == null) throw new IllegalArgumentException("Email is required for new user!");
        // Check if user with this email already exists
        Optional<User> user = userRepo.findUserByEmail(email);
        if(user.isPresent()) {
            // User exists, so reuse the user id
            patient = patient.toBuilder().id(user.get().getId()).build();
        }
        else {
            // Create new user for this user
            User newUser = userRepo.create(User.builder().email(email).build());
            patient = patient.toBuilder().id(newUser.getId()).build();
        }
        Patient newPatient = patientRepo.create(patient);
        metricsProvider.getMeterRegistry().counter("patient_creation").increment();
        tracer.activeSpan().log("Created patient :" + newPatient.getId());
        return newPatient;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Patient> getPatient(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                  @PathParam("id") String id) throws SQLException {
        logger.info("--> Fetching patient with id {}", id);
        Patient queryPatient = Patient.builder().id(id).build();
        return patientRepo.get(queryPatient);
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Patient updatePatient(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           Patient patient) throws SQLException {
        return patientRepo.update(patient);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Patient deletePatient(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           @PathParam("id") String id) throws SQLException {
        return patientRepo.delete(Patient.builder().id(id).build());
    }

    @GET
    @Path("/search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Patient> getPatient(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                    @QueryParam("lastName") String lastName,
                                    @QueryParam("dateOfBirth") String dateOfBirth) throws SQLException, ParseException {
        logger.info("--> Searching patient with lastname {} and date of birth {}", lastName, dateOfBirth);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return patientRepo.findByLastNameOrDOB(lastName, dateFormat.parse(dateOfBirth));
    }

}
