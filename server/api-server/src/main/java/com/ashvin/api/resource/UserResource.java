package com.ashvin.api.resource;

import com.ashvin.platform.domain.Page;
import com.ashvin.platform.domain.User;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.ashvin.platform.repo.UserRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Optional;

@Path("/users")
public class UserResource {

    private Logger logger = LoggerFactory.getLogger(UserResource.class);
    private UserRepo userRepo;
    private MetricsProvider metricsProvider;
    private Tracer tracer;

    @Inject
    public UserResource(DataSource dataSource, MetricsProvider metricsProvider, Tracer tracer) {
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
        this.userRepo = new UserRepo(dataSource, tracer);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<User> getUsers(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                               @BeanParam Page<User> pageRequest) throws SQLException {
        logger.info("--> Fetching all users");
        return userRepo.getAll(tenantId, pageRequest);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User createUser(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           User user) throws SQLException {
        User newUser = userRepo.create(user);
        metricsProvider.getMeterRegistry().counter("user_creation").increment();
        tracer.activeSpan().log("Created user " + newUser.getEmail());
        return newUser;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<User> getUser(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                  @PathParam("id") String id) throws SQLException {
        logger.info("--> Fetching user with id {}", id);
        User queryUser = User.builder().id(id).build();
        return userRepo.get(queryUser);
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User updateUser(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           User user) throws SQLException {
        return userRepo.update(user);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User deleteUser(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           @PathParam("id") String id) throws SQLException {
        return userRepo.delete(User.builder().id(id).build());
    }

}
