package com.ashvin.api.domain;

import com.ashvin.platform.domain.Patient;
import com.ashvin.platform.domain.User;

public class CreatePatientRequest {

    private final User user;
    private final Patient patient;

    public CreatePatientRequest(User user, Patient patient) {
        this.user = user;
        this.patient = patient;
    }

    public User getUser() {
        return user;
    }

    public Patient getPatient() {
        return patient;
    }
}
