package com.ashvin.api.resource;

import com.ashvin.platform.domain.Page;
import com.ashvin.platform.domain.Tenant;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.ashvin.platform.repo.TenantRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Optional;

@Path("/tenants")
public class TenantResource {

    private Logger logger = LoggerFactory.getLogger(TenantResource.class);
    private TenantRepo tenantRepo;
    private MetricsProvider metricsProvider;
    private Tracer tracer;

    @Inject
    public TenantResource(DataSource dataSource, MetricsProvider metricsProvider, Tracer tracer) {
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
        this.tenantRepo = new TenantRepo(dataSource, tracer);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Tenant> getTenants(@BeanParam Page<Tenant> pageRequest) throws SQLException {
        logger.info("--> Fetching all tenants");
        return tenantRepo.getAll(null, pageRequest);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Tenant createTenant(Tenant tenant) throws SQLException {
        Tenant newTenant = tenantRepo.create(tenant);
        metricsProvider.getMeterRegistry().counter("tenant_creation").increment();
        tracer.activeSpan().log("Created tenant " + newTenant.getName());
        return newTenant;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Tenant> getTenant(@PathParam("id") String id) throws SQLException {
        logger.info("--> Fetching tenant with id {}", id);
        Tenant queryTenant = Tenant.builder().id(id).build();
        return tenantRepo.get(queryTenant);
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Tenant updateTenant(Tenant tenant) throws SQLException {
        return tenantRepo.update(tenant);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tenant deleteTenant(@PathParam("id") String id) throws SQLException {
        return tenantRepo.delete(Tenant.builder().id(id).build());
    }

}
