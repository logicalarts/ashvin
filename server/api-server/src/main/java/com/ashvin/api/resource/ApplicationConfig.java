package com.ashvin.api.resource;

import com.ashvin.platform.metrics.resource.BaseResourceConfig;
import io.opentracing.Tracer;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Configuration
@Component
public class ApplicationConfig extends BaseResourceConfig {

    @Inject
    public ApplicationConfig(Tracer tracer){
        super(tracer);
        packages("com.ashvin.api");
        // Register the Feature for Multipart Uploads (File Upload):
        register(MultiPartFeature.class);
    }

}
