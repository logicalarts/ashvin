package com.ashvin.api.domain;

import com.ashvin.platform.domain.Appointment;
import com.ashvin.platform.domain.QueueEntry;

import java.util.Date;

public class QueuePatientRequest {

    private QueueEntry queueEntry;
    private Appointment appointment;

    public QueuePatientRequest(QueueEntry queueEntry, Appointment appointment) {
        this.queueEntry = queueEntry;
        this.appointment = appointment;
    }

    public QueueEntry getQueueEntry() {
        return queueEntry;
    }

    public Appointment getAppointment() {
        return appointment;
    }
}
