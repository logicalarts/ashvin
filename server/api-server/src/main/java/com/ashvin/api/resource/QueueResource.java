package com.ashvin.api.resource;

import com.ashvin.api.domain.QueuePatientRequest;
import com.ashvin.platform.domain.Appointment;
import com.ashvin.platform.domain.Page;
import com.ashvin.platform.domain.QueueEntry;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.ashvin.platform.repo.AppointmentRepo;
import com.ashvin.platform.repo.QueueEntryRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Path("/queues")
public class QueueResource {

    private Logger logger = LoggerFactory.getLogger(QueueResource.class);
    private QueueEntryRepo queueEntryRepo;
    private AppointmentRepo appointmentRepo;
    private MetricsProvider metricsProvider;
    private Tracer tracer;

    @Inject
    public QueueResource(DataSource dataSource, MetricsProvider metricsProvider, Tracer tracer) {
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
        this.queueEntryRepo = new QueueEntryRepo(dataSource, tracer);
        this.appointmentRepo = new AppointmentRepo(dataSource, tracer);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<QueueEntry> getQueueEntries(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                      @BeanParam Page<QueueEntry> pageRequest) throws SQLException {
        logger.info("--> Fetching all queues");
        return queueEntryRepo.getAll(tenantId, pageRequest);
    }

    @GET
    @Path("/day/{workingDay}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<QueueEntry> getQueueEntriesByWorkingDay(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                            @PathParam("workingDay") String workingDay) throws SQLException, ParseException {
        logger.info("--> Fetching all queue entries for working day {}", workingDay);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date workingDate = simpleDateFormat.parse(workingDay);
        return queueEntryRepo.findAllByWorkingDay(tenantId, workingDate);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public QueueEntry createQueueEntry(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                  QueueEntry queueEntry) throws SQLException {
        queueEntry.setTenantId(tenantId);
        QueueEntry newQueueEntry = queueEntryRepo.create(queueEntry);
        metricsProvider.getMeterRegistry().counter("queue_creation").increment();
        tracer.activeSpan().log("Created queue " + newQueueEntry.getId());
        return newQueueEntry;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<QueueEntry> getQueueEntry(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                         @PathParam("id") String id) throws SQLException {
        logger.info("--> Fetching queue with id {}", id);
        QueueEntry queryQueueEntry = QueueEntry.builder().id(id).tenantId(tenantId).build();
        return queueEntryRepo.get(queryQueueEntry);
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public QueueEntry updateQueueEntry(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                  QueueEntry queueEntry) throws SQLException {
        queueEntry.setTenantId(tenantId);
        return queueEntryRepo.update(queueEntry);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public QueueEntry deleteQueueEntry(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                  @PathParam("id") String id) throws SQLException {
        return queueEntryRepo.delete(QueueEntry.builder().id(id).tenantId(tenantId).build());
    }

    @POST
    @Path("/patient")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<QueueEntry> enqueuePatient(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                           QueuePatientRequest queuePatientRequest) throws SQLException {
        Appointment savedAppointment;
        //Check if the appointment exists
        if(queuePatientRequest.getAppointment().getId() == null) {
            //New appointment, save this appointment
            savedAppointment = appointmentRepo.create(queuePatientRequest.getAppointment().toBuilder()
                    .tenantId(tenantId).build());
        }
        else {
            savedAppointment = queuePatientRequest.getAppointment().toBuilder().tenantId(tenantId).build();
        }
        QueueEntry newQueueEntry = queuePatientRequest.getQueueEntry().toBuilder().tenantId(tenantId).appointmentId(savedAppointment.getId()).build();
        QueueEntry savedQueueEntry = queueEntryRepo.create(newQueueEntry);
        metricsProvider.getMeterRegistry().counter("queue_creation").increment();
        tracer.activeSpan().log("Created queue " + newQueueEntry.getId());
        return queueEntryRepo.findAllByWorkingDay(tenantId, savedQueueEntry.getWorkingDay());
    }

}
