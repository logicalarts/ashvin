package com.ashvin.api.resource;

import com.ashvin.platform.domain.Page;
import com.ashvin.platform.domain.Appointment;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.ashvin.platform.repo.AppointmentRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.sql.DataSource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.sql.SQLException;
import java.util.Optional;

@Path("/appointments")
public class AppointmentResource {

    private Logger logger = LoggerFactory.getLogger(AppointmentResource.class);
    private AppointmentRepo appointmentRepo;
    private MetricsProvider metricsProvider;
    private Tracer tracer;

    @Inject
    public AppointmentResource(DataSource dataSource, MetricsProvider metricsProvider, Tracer tracer) {
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
        this.appointmentRepo = new AppointmentRepo(dataSource, tracer);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<Appointment> getAppointments(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                               @BeanParam Page<Appointment> pageRequest) throws SQLException {
        logger.info("--> Fetching all appointments");
        return appointmentRepo.getAll(tenantId, pageRequest);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Appointment createAppointment(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           Appointment appointment) throws SQLException {
        appointment.setTenantId(tenantId);
        Appointment newAppointment = appointmentRepo.create(appointment);
        metricsProvider.getMeterRegistry().counter("appointment_creation").increment();
        tracer.activeSpan().log("Created appointment " + newAppointment.getId());
        return newAppointment;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Optional<Appointment> getAppointment(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                                  @PathParam("id") String id) throws SQLException {
        logger.info("--> Fetching appointment with id {}", id);
        Appointment queryAppointment = Appointment.builder().id(id).tenantId(tenantId).build();
        return appointmentRepo.get(queryAppointment);
    }

    @POST
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Appointment updateAppointment(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           Appointment appointment) throws SQLException {
        appointment.setTenantId(tenantId);
        return appointmentRepo.update(appointment);
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Appointment deleteAppointment(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                           @PathParam("id") String id) throws SQLException {
        return appointmentRepo.delete(Appointment.builder().id(id).tenantId(tenantId).build());
    }

}
