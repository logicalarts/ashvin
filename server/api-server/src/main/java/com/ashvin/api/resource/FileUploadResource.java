package com.ashvin.api.resource;

import com.ashvin.api.async.ObjectProducer;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import io.opentracing.Span;
import io.opentracing.Tracer;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.time.Instant;

@Component
@Path("/upload")
//@PermitAll
public class FileUploadResource {

    private Logger logger = LoggerFactory.getLogger(FileUploadResource.class);
    private ObjectProducer objectProducer;
    private MetricsProvider metricsProvider;
    private Tracer tracer;

    @Inject
    public FileUploadResource(ObjectProducer objectProducer, MetricsProvider metricsProvider, Tracer tracer) {
        this.objectProducer = objectProducer;
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
    }

    @PreAuthorize("#oauth2.hasTenantRole(#tenantId, 'ADMIN')")
    @GET
    public String hello(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId) {
        return "Hello World!";
    }

    @PreAuthorize("#oauth2.hasTenantRole(#tenantId, 'ADMIN')")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFle(@HeaderParam(Constants.HEADER_X_TENANT_ID) String tenantId,
                              @FormDataParam("objectType") String objectType,
                              @FormDataParam("file") InputStream inputStream,
                              @FormDataParam("file") FormDataContentDisposition fileMetadata) {
        Span uploadSpan = tracer.buildSpan("File Upload").asChildOf(tracer.activeSpan()).start();
        uploadSpan.log("Received file for object type " + objectType);
        try {
            File tempFile = File.createTempFile(fileMetadata.getFileName(),
                    String.valueOf(Instant.now().toEpochMilli()));
            OutputStream out = new FileOutputStream(tempFile);
            int readableLength;
            byte[] bytes = new byte[1024];
            while ((readableLength = inputStream.read(bytes)) != -1) {
                out.write(bytes, 0, readableLength);
            }
            out.flush();
            out.close();
            createCollection(objectType, tempFile);
        } catch (IOException e) {
            throw new WebApplicationException("Unable to upload file: " + e.getMessage());
        }
        uploadSpan.log("Processed file for object type " + objectType);
        uploadSpan.finish();
        return Response.ok("File uploaded successfully").build();
    }

    private void createCollection(String objectType, File tempFile) {
        logger.debug("--> Creating object events for: {}", objectType);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode jsonNode = objectMapper.readTree(tempFile);
            if(jsonNode.isArray()) {
                ArrayNode arrayNode = (ArrayNode) jsonNode;
                arrayNode.elements().forEachRemaining(objectNode -> {
                    //try {
                        //Publish the object to Kafka queue
                        //objectProducer.publishObjectEvent(objectType, objectMapper.writeValueAsString(objectNode));
                        metricsProvider.getMeterRegistry().counter("object_import").increment();
//                    } catch (IOException e) {
//                        logger.debug("Unable to publish object message to Kafka: {}", e.getMessage());
//                    }
                });
            }
        } catch (IOException e) {
            logger.debug("Unable to read json from temp file: {}", e.getMessage());
        }
        logger.debug("--> Finished creating object events for: {}", objectType);
    }

}
