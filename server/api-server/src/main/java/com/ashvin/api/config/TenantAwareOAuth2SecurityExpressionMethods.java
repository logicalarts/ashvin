package com.ashvin.api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.expression.OAuth2SecurityExpressionMethods;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TenantAwareOAuth2SecurityExpressionMethods extends OAuth2SecurityExpressionMethods {

    private Logger logger = LoggerFactory.getLogger(TenantAwareOAuth2SecurityExpressionMethods.class);
    private Authentication authentication;

    TenantAwareOAuth2SecurityExpressionMethods(Authentication authentication) {
        super(authentication);
        this.authentication = authentication;
    }

    public boolean hasTenantRole(String tenantId, String role) {
        if(isOAuth()) {
            // Get authentication decoded details
            // Search decoded details for tenantId
            // Compare role with roles allowed for tenant
            Map<String, Object> decodedDetails = Collections.unmodifiableMap((Map<String, Object>) ((OAuth2AuthenticationDetails) authentication.getDetails()).getDecodedDetails());
            List<Map> tenantAuthoritiesList = (List<Map>) decodedDetails.get("tenant_authorities");
            Map<String, Object> tenantMap = tenantAuthoritiesList.stream()
                    .filter(map -> map.get("id").toString().equals(tenantId))
                    .collect(Collectors.toList()).get(0);
            List<String> allowedRoles = (List<String>) tenantMap.get("roles");
            return allowedRoles.contains(role);
        }
        return false;
    }
}
