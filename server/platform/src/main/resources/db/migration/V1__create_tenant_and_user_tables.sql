create table if not exists `tenant` (
    `id` varchar(40) not null primary key,
    `name` varchar(100),
    `description` varchar(250),
    `created` timestamp not null,
    `lastUpdated` timestamp,
    `createdBy` varchar(100),
    `updatedBy` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists `user` (
    `id` varchar(40) not null primary key,
    `email` varchar(254) not null unique,
    `password` varchar(100),
    `status` varchar(20),
    `created` timestamp not null,
    `lastUpdated` timestamp,
    `createdBy` varchar(100),
    `updatedBy` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists `tenant_user` (
    `id` varchar(40) not null primary key,
    `userId` varchar(40) not null,
    `tenantId` varchar(40) not null,
    `roles` varchar(500),
    `created` timestamp not null,
    `lastUpdated` timestamp,
    `createdBy` varchar(100),
    `updatedBy` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

## create default admin user for testing
insert into `tenant`(id, name, description, created) values ('1', 'Dhanvantari Clinic', 'Dhanvantari Clinic', now());
insert into `user`(id, email, password, created) values ('1', 'admin', '{bcrypt}$2a$10$KKIrpZusuooiLZXViewF7OrLCDA.WnO.vAKRqIuHXEL8eFJn3h3Wm', now());
insert into `tenant_user`(id, userId, tenantId, roles, created) values ('1','1', '1', 'ADMIN,USER,DOCTOR,FRONTDESK,PATIENT', now());
insert into `user`(id, email, password, created) values ('2', 'user', '{bcrypt}$2a$10$INxVzg0YRdMON9BnWsxjWOfs8bqPL8yOtipz8MzDYJfPGfJg1yVHO', now());
insert into `tenant_user`(id, userId, tenantId, roles, created) values ('2','2', '1', 'USER', now());
commit;