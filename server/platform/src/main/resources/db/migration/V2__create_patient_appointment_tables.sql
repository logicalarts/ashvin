create table if not exists `patient` (
    `id` varchar(40) not null primary key,
    `firstName` varchar(100),
    `middleName` varchar(100),
    `lastName` varchar(100) not null,
    `dateOfBirth` timestamp not null,
    `lastAppointmentId` varchar(40),
    `notes` varchar(500),
    `created` timestamp not null,
    `lastUpdated` timestamp,
    `createdBy` varchar(100),
    `updatedBy` varchar(100),
    foreign key (id) references user(id) on delete restrict
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists `appointment` (
    `id` varchar(40) not null primary key,
    `tenantId` varchar(40) not null,
    `patientId` varchar(40) not null,
    `checkIn` timestamp not null,
    `checkOut` timestamp,
    `symptoms` varchar(500),
    `diagnosis` varchar(500),
    `prescription` varchar(500),
    `created` timestamp not null,
    `lastUpdated` timestamp,
    `createdBy` varchar(100),
    `updatedBy` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

create table if not exists `queueEntry` (
    `id` varchar(40) not null primary key,
    `tenantId` varchar(40) not null,
    `patientId` varchar(40) not null,
    `appointmentId` varchar(40),
    `workingDay` date,
    `index` smallint,
    `created` timestamp not null,
    `lastUpdated` timestamp,
    `createdBy` varchar(100),
    `updatedBy` varchar(100)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
