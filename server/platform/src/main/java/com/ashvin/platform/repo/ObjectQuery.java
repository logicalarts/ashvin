package com.ashvin.platform.repo;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@Slf4j
public class ObjectQuery {

    private static final String QUERY_VARIABLE_PATTERN = "\\$\\{(\\p{Graph})*}";
    private static final Pattern QUERY_PATTERN = Pattern.compile(QUERY_VARIABLE_PATTERN);

    String baseSqlQuery;
    String dbQuery;
    List<String> parameterNames;

    public ObjectQuery(String sql) {
        this.baseSqlQuery = sql;
        this.dbQuery = baseSqlQuery.replaceAll(QUERY_VARIABLE_PATTERN, " ? ");
        this.parameterNames = findParameterNames(sql);
    }

    private static List<String> findParameterNames(String sql) {
        Matcher matcher = QUERY_PATTERN.matcher(sql);
        List<String> parameterNames = new ArrayList<>();
        while(matcher.find()) {
            parameterNames.add(
                    matcher.group()
                            .replaceAll("\\$\\{", "")
                            .replaceAll("}", ""));
        }
        return parameterNames;
    }

    <T> Object[] findParameterValues(T object) {
        Object[] values = new Object[this.parameterNames.size()];
        for(int i=0; i < values.length; i++) {
            String paramName = parameterNames.get(i);
            try {
                values[i] = PropertyUtils.getProperty(object, paramName);
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                log.error(e.getMessage());
            }
        }
        return  values;
    }


}
