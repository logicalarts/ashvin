package com.ashvin.platform.repo;

import com.ashvin.platform.domain.Appointment;
import com.ashvin.platform.domain.TenantAwareIdentifiable;
import io.opentracing.Tracer;

import javax.sql.DataSource;

public class AppointmentRepo extends AbstractObjectRepo<Appointment, TenantAwareIdentifiable> {

    private ObjectQuery createQuery = new ObjectQuery("insert into `appointment` (id, tenantId, patientId, checkIn, checkOut, symptoms, diagnosis, prescription, created) " +
            "values (${id}, ${tenantId}, ${patientId}, ${checkIn}, ${checkOut}, ${symptoms}, ${diagnosis}, ${prescription}, ${created})");
    private ObjectQuery updateQuery = new ObjectQuery("update `appointment` set patientId = ${patientId}, " +
            "checkIn = ${checkIn}, checkOut = ${checkOut}, symptoms = ${symptoms},  diagnosis = ${diagnosis}, prescription = ${prescription} where tenantId = ${tenantId} and id = ${id}");
    private ObjectQuery deleteQuery = new ObjectQuery("delete from `appointment` where tenantId = ${tenantId} and id = ${id}");
    private ObjectQuery readQuery = new ObjectQuery("select * from `appointment` where tenantId = ${tenantId} and id = ${id}");
    private ObjectQuery findAllQuery = new ObjectQuery("select * from `appointment` where tenantId = ${tenantId}");
    
    public AppointmentRepo(DataSource dataSource, Tracer tracer) {
        super(dataSource, tracer);
    }

    @Override
    public Class<Appointment> getType() {
        return Appointment.class;
    }

    @Override
    public ObjectQuery getCreateQuery() {
        return createQuery;
    }

    @Override
    public ObjectQuery getUpdateQuery() {
        return updateQuery;
    }

    @Override
    public ObjectQuery getDeleteQuery() {
        return deleteQuery;
    }

    @Override
    public ObjectQuery getReadQuery() {
        return readQuery;
    }

    @Override
    public ObjectQuery getFindAllQuery() {
        return findAllQuery;
    }

}

