package com.ashvin.platform.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
abstract class AbstractTenantObject extends AbstractObject implements TenantAwareIdentifiable {

    String tenantId;

}
