package com.ashvin.platform.metrics.resource;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/error")
public class ErrorResource {

    @Inject
    HttpServletRequest request;

    @GET
    public String error() {
        return "error" + request.getRequestURI();
    }
}
