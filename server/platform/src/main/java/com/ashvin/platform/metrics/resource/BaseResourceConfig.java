package com.ashvin.platform.metrics.resource;

import io.opentracing.Tracer;
import io.opentracing.contrib.jaxrs2.server.ServerTracingDynamicFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

public abstract class BaseResourceConfig extends ResourceConfig {

    public BaseResourceConfig(Tracer tracer) {
        packages("com.ashvin.platform");
        register(new ServerTracingDynamicFeature.Builder(tracer).build());
        property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true);
    }

}
