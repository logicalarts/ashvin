package com.ashvin.platform.repo;

import com.ashvin.platform.domain.Identifiable;
import com.ashvin.platform.domain.TenantUser;
import io.opentracing.Tracer;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.List;

public class TenantUserRepo extends AbstractObjectRepo<TenantUser, Identifiable> {

    private ObjectQuery createQuery = new ObjectQuery("insert into `tenant_user` (id, userId, tenantId, roles, created) " +
            "values (${id}, ${userId}, ${tenantId}, ${roles},  ${created})");
    private ObjectQuery updateQuery = new ObjectQuery("update `tenant_user` set userId = ${userId}, " +
            "tenantId = ${tenantId}, roles = ${roles} where id = ${id}");
    private ObjectQuery deleteQuery = new ObjectQuery("delete from `tenant_user` where id = ${id}");
    private ObjectQuery readQuery = new ObjectQuery("select * from `tenant_user` where id = ${id}");
    private ObjectQuery findAllQuery = new ObjectQuery("select * from `tenant_user`");
    private ObjectQuery findByUserIdQuery = new ObjectQuery("select * from `tenant_user` where userId = ${userId}");

    public TenantUserRepo(DataSource dataSource, Tracer tracer) {
        super(dataSource, tracer);
    }

    public List<TenantUser> findByUserId(String userId) throws SQLException {
        TenantUser tenantUser = TenantUser.builder().userId(userId).build();
        return findManyByQuery(tenantUser, findByUserIdQuery);
    }


    @Override
    public Class<TenantUser> getType() {
        return TenantUser.class;
    }

    @Override
    public ObjectQuery getCreateQuery() {
        return createQuery;
    }

    @Override
    public ObjectQuery getUpdateQuery() {
        return updateQuery;
    }

    @Override
    public ObjectQuery getDeleteQuery() {
        return deleteQuery;
    }

    @Override
    public ObjectQuery getReadQuery() {
        return readQuery;
    }

    @Override
    public ObjectQuery getFindAllQuery() {
        return findAllQuery;
    }


}
