package com.ashvin.platform.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.QueryParam;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> {

    public enum SortOrder { asc, desc}

    List<T> objects;
    @QueryParam("number")
    int number;
    @QueryParam("size")
    int size;
    @QueryParam("sortOrder")
    SortOrder sortOrder;
    @QueryParam("sortBy")
    String sortBy;

}
