package com.ashvin.platform.metrics.resource;

import com.ashvin.platform.metrics.provider.MetricsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Singleton
@Path("/metrics")
public class MetricsResource {

    private Logger logger = LoggerFactory.getLogger(MetricsResource.class);
    private MetricsProvider metricsProvider;

    @Inject
    public MetricsResource(MetricsProvider metricsProvider) {
        this.metricsProvider = metricsProvider;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getMetrics() {
        logger.info("--> Providing metrics");
        return metricsProvider.scrape();
    }

}
