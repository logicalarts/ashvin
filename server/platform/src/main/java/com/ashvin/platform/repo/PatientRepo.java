package com.ashvin.platform.repo;

import com.ashvin.platform.domain.Identifiable;
import com.ashvin.platform.domain.Patient;
import io.opentracing.Tracer;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class PatientRepo extends AbstractObjectRepo<Patient, Identifiable> {

    private ObjectQuery createQuery = new ObjectQuery("insert into `patient` (id, firstName, lastName, middleName, dateOfBirth, notes, created) " +
            "values (${id}, ${firstName}, ${lastName}, ${middleName}, ${dateOfBirth}, ${notes}, ${created})");
    private ObjectQuery updateQuery = new ObjectQuery("update `patient` set firstName = ${firstName}, " +
            "lastName = ${lastName}, middleName = ${middleName}, dateOfBirth = ${dateOfBirth},  notes = ${notes} where id = ${id}");
    private ObjectQuery deleteQuery = new ObjectQuery("delete from `patient` where id = ${id}");
    private ObjectQuery readQuery = new ObjectQuery("select * from `patient` where id = ${id}");
    private ObjectQuery findAllQuery = new ObjectQuery("select * from `patient`");
    private ObjectQuery findByLastNameOrDOBQuery = new ObjectQuery("select * from `patient` where lastName like ${lastName} or dateOfBirth = ${dateOfBirth}");

    public PatientRepo(DataSource dataSource, Tracer tracer) {
        super(dataSource, tracer);
    }

    public List<Patient> findByLastNameOrDOB(String lastNameLike, Date dateOfBirth) throws SQLException {
        Patient patient = Patient.builder().dateOfBirth(dateOfBirth).lastName("%" + lastNameLike + "%").build();
        return findManyByQuery(patient, findByLastNameOrDOBQuery);
    }

    @Override
    public Class<Patient> getType() {
        return Patient.class;
    }

    @Override
    public ObjectQuery getCreateQuery() {
        return createQuery;
    }

    @Override
    public ObjectQuery getUpdateQuery() {
        return updateQuery;
    }

    @Override
    public ObjectQuery getDeleteQuery() {
        return deleteQuery;
    }

    @Override
    public ObjectQuery getReadQuery() {
        return readQuery;
    }

    @Override
    public ObjectQuery getFindAllQuery() {
        return findAllQuery;
    }


}
