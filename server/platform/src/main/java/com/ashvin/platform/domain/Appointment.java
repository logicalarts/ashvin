package com.ashvin.platform.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
public class Appointment extends AbstractTenantObject {

    String patientId;
    Date checkIn;
    Date checkOut;
    String symptoms;
    String diagnosis;
    String prescription;

}
