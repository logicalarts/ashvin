package com.ashvin.platform.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@SuperBuilder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractObject implements Identifiable {

    String id;
    Date created;
    Date lastUpdated;
    String createdBy;
    String updatedBy;

}
