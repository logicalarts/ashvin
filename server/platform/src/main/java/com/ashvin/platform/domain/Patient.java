package com.ashvin.platform.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder(toBuilder=true)
@AllArgsConstructor
@NoArgsConstructor
public class Patient extends AbstractObject {

    String firstName;
    String middleName;
    String lastName;
    Date dateOfBirth;
    String lastAppointmentId;
    String notes;

}
