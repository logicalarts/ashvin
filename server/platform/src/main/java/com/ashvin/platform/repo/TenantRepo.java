package com.ashvin.platform.repo;

import com.ashvin.platform.domain.Identifiable;
import com.ashvin.platform.domain.Tenant;
import io.opentracing.Tracer;

import javax.sql.DataSource;

public class TenantRepo extends AbstractObjectRepo<Tenant, Identifiable> {

    private ObjectQuery createQuery = new ObjectQuery("insert into tenant (id, name, description, created) " +
            "values (${id}, ${name}, ${description}, ${created})");
    private ObjectQuery updateQuery = new ObjectQuery("update tenant set name = ${name}, " +
            "description = ${description} where id = ${id}");
    private ObjectQuery deleteQuery = new ObjectQuery("delete from tenant where id = ${id}");
    private ObjectQuery readQuery = new ObjectQuery("select * from tenant where id = ${id}");
    private ObjectQuery findAllQuery = new ObjectQuery("select * from tenant");

    public TenantRepo(DataSource dataSource, Tracer tracer) {
        super(dataSource, tracer);
    }

    @Override
    public Class<Tenant> getType() {
        return Tenant.class;
    }

    @Override
    public ObjectQuery getCreateQuery() {
        return this.createQuery;
    }

    @Override
    public ObjectQuery getUpdateQuery() {
        return this.updateQuery;
    }

    @Override
    public ObjectQuery getDeleteQuery() {
        return this.deleteQuery;
    }

    @Override
    public ObjectQuery getReadQuery() {
        return this.readQuery;
    }

    @Override
    public ObjectQuery getFindAllQuery() {
        return this.findAllQuery;
    }
}
