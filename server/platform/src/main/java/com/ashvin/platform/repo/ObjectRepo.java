package com.ashvin.platform.repo;

import com.ashvin.platform.domain.AbstractObject;
import com.ashvin.platform.domain.Identifiable;
import com.ashvin.platform.domain.Page;
import com.ashvin.platform.domain.TenantAwareIdentifiable;
import lombok.Cleanup;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public interface ObjectRepo<T extends AbstractObject, ID extends Identifiable> {

    Logger log = LoggerFactory.getLogger(ObjectRepo.class);

    Class<T> getType();

    Connection getDBConnection() throws SQLException;

    ObjectQuery getCreateQuery();

    ObjectQuery getUpdateQuery();

    ObjectQuery getDeleteQuery();

    ObjectQuery getReadQuery();

    ObjectQuery getFindAllQuery();

    default T create(T object) throws SQLException {
        if(object.getId() == null) {
            object.setId(UUID.randomUUID().toString());
        }
        object.setCreated(new Date());
        ObjectQuery query = getCreateQuery();
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        Object[] parameterValues = query.findParameterValues(object);
        log.info("--> Executing query: {} with parameters: {}", query.getDbQuery(), parameterValues);
        queryRunner.insert(connection, query.getDbQuery(), new MapHandler(), parameterValues);
        return object;
    }

    default T update(T object) throws SQLException {
        ObjectQuery query = getUpdateQuery();
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        Object[] parameterValues = query.findParameterValues(object);
        log.info("--> Executing query: {} with parameters: {}", query.getDbQuery(), parameterValues);
        queryRunner.update(connection, query.getDbQuery(), parameterValues);
        return object;
    }

    default Optional<T> get(ID id) throws SQLException {
        ObjectQuery query = getReadQuery();
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        Object[] parameterValues;
        if(id instanceof TenantAwareIdentifiable) {
            TenantAwareIdentifiable tenantAwareIdentifiable = (TenantAwareIdentifiable) id;
            parameterValues = new Object[2];
            parameterValues[0] = tenantAwareIdentifiable.getTenantId();
            parameterValues[1] = tenantAwareIdentifiable.getId();
        }
        else {
            parameterValues = new Object[1];
            parameterValues[0] = id.getId();
        }
        log.info("--> Executing query: {} with parameters: {}", query.getDbQuery(), parameterValues);
        return Optional.ofNullable(queryRunner.query(connection, query.getDbQuery(), new BeanHandler<>(getType()), parameterValues));
    }

    default T delete(T object) throws SQLException {
        ObjectQuery query = getDeleteQuery();
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        Object[] parameterValues = query.findParameterValues(object);
        log.info("--> Executing query: {} with parameters: {}", query.getDbQuery(), parameterValues);
        queryRunner.execute(connection, query.getDbQuery(), new MapHandler(), parameterValues);
        return object;
    }

    default Page<T> getAll(String tenantId, Page<T> pageRequest) throws SQLException {
        ObjectQuery query = getFindAllQuery();
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        StringBuilder pageQueryBuilder = new StringBuilder(query.getDbQuery());
        if(pageRequest.getSortBy() != null) {
            pageQueryBuilder.append(" ORDER BY ")
                    .append(pageRequest.getSortBy())
                    .append(" ")
                    .append(pageRequest.getSortOrder() != null ? pageRequest.getSortOrder() : "");
                    //.append(" NULLS LAST ")
        }
        pageQueryBuilder
                .append(" LIMIT ")
                .append(pageRequest.getSize())
                .append(" OFFSET ")
                .append(pageRequest.getNumber() * pageRequest.getSize());
        String dbQuery =  pageQueryBuilder.toString();
        log.info("--> Executing query: {}", dbQuery);
        List<T> objects;
        if(tenantId != null) {
            objects = queryRunner.query(connection, dbQuery, new BeanListHandler<>(getType()), tenantId);
        }
        else {
            objects = queryRunner.query(connection, dbQuery, new BeanListHandler<>(getType()));
        }
        return Page.<T>builder()
                .number(pageRequest.getNumber())
                .size(pageRequest.getSize())
                .sortBy(pageRequest.getSortBy())
                .sortOrder(pageRequest.getSortOrder())
                .objects(objects)
                .build();
    }

    default Optional<T> findByQuery(T example, ObjectQuery query) throws SQLException {
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        Object[] parameterValues = query.findParameterValues(example);
        log.info("--> Executing query: {} with parameters: {}", query.getDbQuery(), parameterValues);
        return Optional.ofNullable(queryRunner.query(connection, query.getDbQuery(), new BeanHandler<>(getType()), parameterValues));
    }

    default List<T> findManyByQuery(T example, ObjectQuery query) throws SQLException {
        @Cleanup Connection connection = getDBConnection();
        QueryRunner queryRunner = new QueryRunner();
        Object[] parameterValues = query.findParameterValues(example);
        log.info("--> Executing query: {} with parameters: {}", query.getDbQuery(), parameterValues);
        return queryRunner.query(connection, query.getDbQuery(), new BeanListHandler<>(getType()), parameterValues);
    }

}
