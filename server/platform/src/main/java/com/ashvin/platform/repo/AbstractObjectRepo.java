package com.ashvin.platform.repo;

import com.ashvin.platform.domain.AbstractObject;
import com.ashvin.platform.domain.Identifiable;
import io.opentracing.Tracer;
import io.opentracing.contrib.jdbc.ConnectionInfo;
import io.opentracing.contrib.jdbc.TracingConnection;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;

public abstract class AbstractObjectRepo<T extends AbstractObject, ID extends Identifiable> implements ObjectRepo<T, ID> {

    private DataSource dataSource;
    private Tracer tracer;

    AbstractObjectRepo(DataSource dataSource, Tracer tracer) {
        this.dataSource = dataSource;
        this.tracer = tracer;
    }

    @Override
    public Connection getDBConnection() throws SQLException {
        Connection dbConnection = dataSource.getConnection();
        dbConnection.setAutoCommit(true);
        if(tracer != null) {
            return new TracingConnection(dbConnection,
                    new ConnectionInfo.Builder(dbConnection.getMetaData().getDatabaseProductName()).build(),
                    false, Collections.emptySet(), tracer);
        }
        return dbConnection;
    }

}
