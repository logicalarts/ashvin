package com.ashvin.platform.repo;

import com.ashvin.platform.domain.Identifiable;
import com.ashvin.platform.domain.User;
import io.opentracing.Tracer;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Optional;

public class UserRepo extends AbstractObjectRepo<User, Identifiable> {

    private ObjectQuery createQuery = new ObjectQuery("insert into `user` (id, email, password, created) " +
            "values (${id}, ${email}, ${password}, ${created})");
    private ObjectQuery updateQuery = new ObjectQuery("update `user` set email = ${email}, " +
            "password = ${password} where id = ${id}");
    private ObjectQuery deleteQuery = new ObjectQuery("delete from `user` where id = ${id}");
    private ObjectQuery readQuery = new ObjectQuery("select * from `user` where id = ${id}");
    private ObjectQuery findAllQuery = new ObjectQuery("select * from `user`");
    private ObjectQuery findByEmailQuery = new ObjectQuery("select * from `user` where email = ${email}");

    public UserRepo(DataSource dataSource, Tracer tracer) {
        super(dataSource, tracer);
    }

    public Optional<User> findUserByEmail(String email) throws SQLException {
        User user = User.builder().email(email).build();
        return findByQuery(user, findByEmailQuery);
    }

    @Override
    public Class<User> getType() {
        return User.class;
    }

    @Override
    public ObjectQuery getCreateQuery() {
        return createQuery;
    }

    @Override
    public ObjectQuery getUpdateQuery() {
        return updateQuery;
    }

    @Override
    public ObjectQuery getDeleteQuery() {
        return deleteQuery;
    }

    @Override
    public ObjectQuery getReadQuery() {
        return readQuery;
    }

    @Override
    public ObjectQuery getFindAllQuery() {
        return findAllQuery;
    }

}
