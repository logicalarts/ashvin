package com.ashvin.platform.repo;

import com.ashvin.platform.domain.QueueEntry;
import com.ashvin.platform.domain.TenantAwareIdentifiable;
import io.opentracing.Tracer;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

public class QueueEntryRepo extends AbstractObjectRepo<QueueEntry, TenantAwareIdentifiable> {

    private ObjectQuery createQuery = new ObjectQuery("insert into `queueEntry` (id, tenantId, patientId, appointmentId, `index`, workingDay, created) " +
            "values (${id}, ${tenantId}, ${patientId}, ${appointmentId}, ${index}, ${workingDay}, ${created})");
    private ObjectQuery updateQuery = new ObjectQuery("update `queueEntry` set patientId = ${patientId}, " +
            "appointmentId = ${appointmentId}, `index` = ${index}, workingDay = ${workingDay} where tenantId = ${tenantId} and id = ${id}");
    private ObjectQuery deleteQuery = new ObjectQuery("delete from `queueEntry` where tenantId = ${tenantId} and id = ${id}");
    private ObjectQuery readQuery = new ObjectQuery("select * from `queueEntry` where tenantId = ${tenantId} and id = ${id}");
    private ObjectQuery findAllQuery = new ObjectQuery("select * from `queueEntry` where tenantId = ${tenantId}");
    private ObjectQuery findAllByWorkingDayQuery = new ObjectQuery("select `queueEntry`.* from `queueEntry` " +
            " join `appointment` on `queueEntry`.appointmentId = `appointment`.id " +
            " where `queueEntry`.tenantId = ${tenantId} and date(workingDay) = date(${workingDay}) " +
            " and `appointment`.checkOut is null ");

    public QueueEntryRepo(DataSource dataSource, Tracer tracer) {
        super(dataSource, tracer);
    }

    public List<QueueEntry> findAllByWorkingDay(String tenantId, Date workingDay) throws SQLException {
        QueueEntry example = QueueEntry.builder().tenantId(tenantId).workingDay(workingDay).build();
        return findManyByQuery(example, findAllByWorkingDayQuery);
    }

    @Override
    public Class<QueueEntry> getType() {
        return QueueEntry.class;
    }

    @Override
    public ObjectQuery getCreateQuery() {
        return createQuery;
    }

    @Override
    public ObjectQuery getUpdateQuery() {
        return updateQuery;
    }

    @Override
    public ObjectQuery getDeleteQuery() {
        return deleteQuery;
    }

    @Override
    public ObjectQuery getReadQuery() {
        return readQuery;
    }

    @Override
    public ObjectQuery getFindAllQuery() {
        return findAllQuery;
    }

}
