package com.ashvin.platform.domain;

public interface Identifiable {

    String getId();

}
