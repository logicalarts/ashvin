package com.ashvin.platform.domain;

public interface TenantAwareIdentifiable extends Identifiable {

    String getTenantId();

}
