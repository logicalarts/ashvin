package com.ashvin.platform.repo

import com.ashvin.platform.domain.Tenant
import com.ashvin.platform.domain.TenantUser
import com.ashvin.platform.domain.User
import spock.lang.Requires
import spock.lang.Shared
import spock.lang.Specification

import java.sql.Connection

@Requires({RepoTestUtils.isDBAvailable()})
class TenantUserRepoSpec extends Specification {

    @Shared
    TenantUserRepo tenantUserRepo
    @Shared
    def userRepo = new UserRepoSpec.UserRepoWithConnection()
    def tenantRepo = new TenantRepoSpec.TenantRepoWithConnection()

    def setupSpec() {
        tenantUserRepo = new TenantUserRepoWithConnection()
    }

    def "Assigning tenant roles to user succeeds"() {
        given:
        def user = createUser()
        def tenant = createTenant()
        def tenantUser = TenantUser.builder().userId(user.id).tenantId(tenant.id).roles("ADMIN,USER").build()

        when:
        def result = tenantUserRepo.create(tenantUser)
        def optionalReadResult = tenantUserRepo.get(result)
        def readResult = optionalReadResult.get()

        then:
        readResult != null
        readResult.id != null
        readResult.userId == tenantUser.userId
        readResult.tenantId == tenantUser.tenantId
        readResult.roles == tenantUser.roles
    }

    def "Get tenant roles for a user succeeds"() {
        given:
        def user = createUser()
        def tenant = createTenant()
        def tenantUser = TenantUser.builder().userId(user.id).tenantId(tenant.id).roles("ADMIN,USER").build()

        when:
        def result = tenantUserRepo.create(tenantUser)
        def tenantUsers = tenantUserRepo.findByUserId(result.userId)
        def readResult = tenantUsers.get(0)

        then:
        tenantUsers.size() > 0
        readResult != null
        readResult.id != null
        readResult.userId == tenantUser.userId
        readResult.tenantId == tenantUser.tenantId
        readResult.roles == tenantUser.roles
    }

    def createUser() {
        def user = User.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        user.email = "Test_user_" + randomUUID
        user.password = "Test_user_" + randomUUID
        userRepo.create(user)
    }

    def createTenant() {
        def tenant = Tenant.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        tenant.name = "Test_tenant_" + randomUUID
        tenant.description = "Test_tenant_" + randomUUID
        tenantRepo.create(tenant)
    }

    class TenantUserRepoWithConnection extends TenantUserRepo {

        TenantUserRepoWithConnection(){
            super(null, null)
        }

        @Override
        Connection getDBConnection() {
            return RepoTestUtils.getDBConnection()
        }
    }
}
