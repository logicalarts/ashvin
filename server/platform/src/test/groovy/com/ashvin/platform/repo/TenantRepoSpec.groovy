package com.ashvin.platform.repo

import com.ashvin.platform.domain.Page
import com.ashvin.platform.domain.Tenant
import spock.lang.Requires
import spock.lang.Shared
import spock.lang.Specification

import javax.sql.DataSource
import java.sql.Connection

@Requires({RepoTestUtils.isDBAvailable()})
class TenantRepoSpec extends Specification {

    @Shared
    def tenantRepo

    def setupSpec() {
        tenantRepo = new TenantRepoWithConnection()
    }

    def "Create tenant succeeds"() {
        given:
        def tenant = Tenant.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        tenant.name = "Test_tenant_" + randomUUID
        tenant.description = "Test_tenant_" + randomUUID

        when:
        def result = tenantRepo.create(tenant)

        then:
        result != null
        result.id != null
        result.name == tenant.name
        result.description == tenant.description
    }

    def "Get tenant and Update tenant succeeds"() {
        given:
        def tenant = Tenant.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        tenant.name = "Test_tenant_" + randomUUID
        tenant.description = "Test_tenant_" + randomUUID
        tenantRepo.create(tenant)
        def createdTenant = Tenant.builder().build()
        createdTenant.id = tenant.id

        when:
        createdTenant.name = tenant.name + "_updated"
        createdTenant.description = tenant.description + "_updated"
        tenantRepo.update(createdTenant)
        def queryTenant = createdTenant.toBuilder().build()
        queryTenant.id = createdTenant.id
        def result = tenantRepo.get(queryTenant).get()

        then:
        result != null
        result.id != null
        result.name != tenant.name
        result.description != tenant.description
        result.name == createdTenant.name
        result.description == createdTenant.description
    }

    def "Delete tenant succeeds"() {
        given:
        def tenant = Tenant.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        tenant.name = "Test_tenant_" + randomUUID
        tenant.description = "Test_tenant_" + randomUUID
        tenantRepo.create(tenant)
        def createdTenant = Tenant.builder().build()
        createdTenant.id = tenant.id

        when:
        tenantRepo.delete(createdTenant)
        def queryTenant = createdTenant.toBuilder().build()
        queryTenant.id = createdTenant.id
        def result = tenantRepo.get(queryTenant)

        then:
        result == Optional.empty()
    }

    def "Find all tenants succeeds"() {
        given:
        def tenant = Tenant.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        tenant.name = "Test_tenant_" + randomUUID
        tenant.description = "Test_tenant_" + randomUUID
        tenantRepo.create(tenant)
        def createdTenant = Tenant.builder().build()
        createdTenant.id = tenant.id

        when:
        Page<Tenant> pageRequest = Page.<Tenant>builder().size(10).number(0).sortBy("name").sortOrder(Page.SortOrder.asc).build()
        def tenantsPage = tenantRepo.getAll(null, pageRequest)

        then:
        tenantsPage != null
        tenantsPage.size == 10
        tenantsPage.number == 0
    }

    class TenantRepoWithConnection extends TenantRepo {

        DataSource dataSource

        TenantRepoWithConnection(){
            super(null, null)
        }

        @Override
        Connection getDBConnection() {
            return RepoTestUtils.getDBConnection()
        }
    }

}