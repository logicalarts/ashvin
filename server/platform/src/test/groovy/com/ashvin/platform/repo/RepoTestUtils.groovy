package com.ashvin.platform.repo

import groovy.sql.Sql
import org.flywaydb.core.Flyway

import java.sql.Connection

class RepoTestUtils {

    public static final String DB_URL = "jdbc:mysql://db.ashvin.com:3306/db?autoreconnect=true"
    public static final String DB_USER = "user"
    public static final String DB_PASSWORD = "password"
    public static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver"

    static boolean isDBAvailable() {
        try {
            // Check if DB is available to execute tests
            if(getDBConnection() != null) {
                // DB is available
                // Migrate DB schema using Flyway
                Flyway flyway = Flyway.configure()
                        .dataSource(DB_URL, DB_USER, DB_PASSWORD)
                        .locations("classpath:/db/migration")
                        .load()
                //flyway.repair()
                // !!! DO THIS ONLY FOR TESTING !!!
                // flyway.clean()
                flyway.migrate()
                return true
            }
        }
        catch (Exception ignored){
            return false
        }
        return false
    }

    static Connection getDBConnection() {
        def sql = Sql.newInstance(DB_URL, DB_USER, DB_PASSWORD, DB_DRIVER)
        return sql.getConnection()
    }

}
