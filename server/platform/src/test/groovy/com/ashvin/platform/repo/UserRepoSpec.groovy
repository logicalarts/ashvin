package com.ashvin.platform.repo

import com.ashvin.platform.domain.Page
import com.ashvin.platform.domain.User
import spock.lang.Requires
import spock.lang.Shared
import spock.lang.Specification

import java.sql.Connection

@Requires({RepoTestUtils.isDBAvailable()})
class UserRepoSpec extends Specification {

    @Shared
    def userRepo

    def setupSpec() {
        userRepo = new UserRepoWithConnection()
    }

    def "Create user succeeds"() {
        given:
        def user = User.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        user.email = "Test_user_" + randomUUID
        user.password = "Test_user_" + randomUUID

        when:
        def result = userRepo.create(user)

        then:
        result != null
        result.id != null
        result.email == user.email
        result.password == user.password
    }

    def "Get user and Update user succeeds"() {
        given:
        def user = User.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        user.email = "Test_user_" + randomUUID
        user.password = "Test_user_" + randomUUID
        userRepo.create(user)
        def createdUser = User.builder().build()
        createdUser.id = user.id

        when:
        createdUser.email = user.email + "_updated"
        createdUser.password = user.password + "_updated"
        userRepo.update(createdUser)
        def queryUser = createdUser.toBuilder().build()
        queryUser.id = createdUser.id
        def result = userRepo.get(queryUser).get()

        then:
        result != null
        result.id != null
        result.email != user.email
        result.password != user.password
        result.email == createdUser.email
        result.password == createdUser.password
    }

    def "Delete user succeeds"() {
        given:
        def user = User.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        user.email = "Test_user_" + randomUUID
        user.password = "Test_user_" + randomUUID
        userRepo.create(user)
        def createdUser = User.builder().build()
        createdUser.id = user.id

        when:
        userRepo.delete(createdUser)
        def queryUser = createdUser.toBuilder().build()
        queryUser.id = createdUser.id
        def result = userRepo.get(queryUser)

        then:
        result == Optional.empty()
    }

    def "Find all users succeeds"() {
        given:
        def user = User.builder().build()
        def randomUUID = UUID.randomUUID().toString()
        user.email = "Test_user_" + randomUUID
        user.password = "Test_user_" + randomUUID
        userRepo.create(user)
        def createdUser = User.builder().build()
        createdUser.id = user.id

        when:
        Page<User> pageRequest = Page.<User>builder().size(10).number(0).sortBy("email").sortOrder(Page.SortOrder.asc).build()
        def usersPage = userRepo.getAll(null, pageRequest)

        then:
        usersPage != null
        usersPage.size == 10
        usersPage.number == 0
        usersPage.objects.get(0) != null
        usersPage.objects.get(0).email != null
        usersPage.objects.get(0).password != null
    }

    class UserRepoWithConnection extends UserRepo {

        UserRepoWithConnection(){
            super(null, null)
        }

        @Override
        Connection getDBConnection() {
            return RepoTestUtils.getDBConnection()
        }
    }

}
