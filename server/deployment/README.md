**Minikube setup**

* Install Minikube and KVM2 Driver
* Start Minikube

    `minikube start --vm-driver=kvm2`
    
* Enable Ingress addon for Minikube

    `minikube addons enable ingress`
    
**CI/CD**
* Install 'infra' helm chart
    
    `cd ${repo}/server/deployment`

    `helm install infra -n ashvin-infra`

* Install skaffold

* Use skaffold to deploy app to minikube

    `cd ${repo}/server`

    `skaffold dev --trigger notify` or `skaffold deploy` 


**Deploying app to Minikube**

* Run Helm chart for ashvin app

    `helm install ashvin -n ashvin-app`

* List Helm releases
    
    `helm list`
    
* Delete a release
    
    `helm delete {release name}`
    
**Starting Apache Kafka using Docker containers**
* Use following command to start ZooKeeper and Kafka together in single container(ADVERTISED_HOST=0.0.0.0 binds to all interfaces)

    `docker run -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=0.0.0.0 --env ADVERTISED_PORT=9092 spotify/kafka`
    
* Enable promiscuous mode for docker interface in minikube
    
    `minikube ssh eval $(sudo ip link set docker0 promisc on)` 
    
**Apache Flink deployment**
* K8s deployment for Flink is documented here - https://ci.apache.org/projects/flink/flink-docs-release-1.7/ops/deployment/kubernetes.html

* Deploying Helm chart for ashvin app will deploy Flink components in K8s.

* You can then access the Flink UI via kubectl proxy:
  
  - Run `kubectl proxy` in a terminal
  - Navigate to `http://localhost:8001/api/v1/namespaces/default/services/flink-jobmanager:ui/proxy` in your browser

* To run ObjectStreamProcessorApplication as Flink job, use _'Submit Job'_ interface from the Flink job manager UI.
    - Use shadow jar created via `object-stream-processor:shadowJar` target.
    
