#!/bin/bash
kubectl create -f ./k8s/db-deployment.yml
kubectl create -f ./k8s/db-service.yml
kubectl create -f ./k8s/api-deployment.yml
kubectl create -f ./k8s/api-service.yml
kubectl create -f ./k8s/api-ingress.yml