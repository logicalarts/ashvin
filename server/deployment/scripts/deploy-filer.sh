#!/bin/bash
set -x
#Set docker env to minikube
eval $(minikube docker-env)

#Delete existing service and pod
kubectl delete service ashvin-api
kubectl delete deployment ashvin-api

#Build app and image
pushd .;
cd ../../
pwd
./gradlew api:build api:jibDockerBuild
popd;

#Deploy pod and service
kubectl create -f ../k8s/api-deployment.yml
kubectl create -f ../k8s/api-service.yml

set +x