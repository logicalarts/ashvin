#!/bin/bash
kubectl delete ingress ashvin-api
kubectl delete service ashvin-api
kubectl delete deployment ashvin-api
kubectl delete service ashvin-db
kubectl delete deployment ashvin-db
