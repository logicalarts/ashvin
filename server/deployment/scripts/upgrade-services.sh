#!/bin/bash
set -x
#Set docker env to minikube
eval $(minikube docker-env)

#Delete old release
pushd .;
#Clear out unused docker images
docker image prune -f
cd ..
helm list
helm delete --purge ashvin-app
popd;

#Build app images and push to minikube docker daemon
pushd .;
cd ../../
pwd
./gradlew api:build api:jibDockerBuild
./gradlew object-processor:build object-processor:jibDockerBuild
popd;

#Deploy new release
pushd .;
cd ..
helm list
helm install ashvin --name ashvin-app
popd;
set +x
