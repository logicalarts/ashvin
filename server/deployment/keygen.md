**Sprinb boot:**
Generate PKCS12 keys and store in keystore
`keytool -genkeypair -alias ashvin -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore ashvin.p12 -validity 3650`


**Kubernetes:**
Generate certification and key for ingress
openssl req -newkey rsa:2048 -nodes -keyout ashvin.key -x509 -days 3650 -out ashvin.crt

