package com.ashvin.authz

import spock.lang.Specification

class AuthorizationServerSpec extends Specification {

    def "Test framework works"() {
        given:
        def greeting = "Hello"

        when:
        def name = " Spock"
        //wait(2000)

        then:
        greeting + name == "Hello Spock"
        1 == 1
    }

}
