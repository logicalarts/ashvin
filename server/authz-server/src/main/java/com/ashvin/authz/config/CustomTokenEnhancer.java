package com.ashvin.authz.config;

import com.ashvin.platform.domain.Tenant;
import com.ashvin.platform.domain.TenantAuthority;
import com.ashvin.platform.domain.TenantUser;
import com.ashvin.platform.domain.User;
import com.ashvin.platform.repo.TenantRepo;
import com.ashvin.platform.repo.TenantUserRepo;
import com.ashvin.platform.repo.UserRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.*;

@Configuration
@Service
public class CustomTokenEnhancer implements TokenEnhancer {

    private Logger logger = LoggerFactory.getLogger(CustomTokenEnhancer.class);
    private TenantRepo tenantRepo;
    private UserRepo userRepo;
    private TenantUserRepo tenantUserRepo;

    @Inject
    public CustomTokenEnhancer(DataSource dataSource, Tracer tracer) {
        this.tenantRepo = new TenantRepo(dataSource, tracer);
        this.userRepo = new UserRepo(dataSource, tracer);
        this.tenantUserRepo = new TenantUserRepo(dataSource, tracer);
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>();
        // Populate authorities for tenants from scope as { tenantId : 1, tenantName: "test tenant", roles : ['ADMIN', 'USER']}
        try {
            Optional<User> optionalUser = userRepo.findUserByEmail(authentication.getPrincipal().toString());
            if(optionalUser.isPresent()) {
                User dbUser = optionalUser.get();
                // Find all tenant to user mappings
                List<TenantUser> tenantUsers = tenantUserRepo.findByUserId(dbUser.getId());
                // Collect roles from each tenant
                List<TenantAuthority> allTenantAuthorities = new ArrayList<>();
                tenantUsers.forEach(tenantUser -> {
                    String[] tenantRoles = tenantUser.getRoles().split(",");
                    try {
                        Optional<Tenant> optionalTenant = tenantRepo.get(Tenant.builder().id(tenantUser.getTenantId()).build());
                        if(optionalTenant.isPresent()) {
                            Tenant tenant = optionalTenant.get();
                            allTenantAuthorities.add(new TenantAuthority(tenantUser.getTenantId(), tenant.getName(), tenantRoles));
                        }
                    } catch (SQLException e) {
                        logger.error(e.getMessage());
                    }
                });
                additionalInfo.put("tenant_authorities", allTenantAuthorities);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }
        ((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
