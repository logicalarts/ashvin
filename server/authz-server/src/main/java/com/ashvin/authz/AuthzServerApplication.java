package com.ashvin.authz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ashvin")
public class AuthzServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthzServerApplication.class, args);
    }

}
