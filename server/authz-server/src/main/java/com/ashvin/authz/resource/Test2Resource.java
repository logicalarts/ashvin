package com.ashvin.authz.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class Test2Resource {

    @GetMapping("/test2")
    public String test2Handler(HttpServletRequest request) {
        return "Test2";
    }
}
