package com.ashvin.authz.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class TestResource {

    @GetMapping("/test")
    public String testResource(HttpServletRequest request) {
        return "test";
    }

}
