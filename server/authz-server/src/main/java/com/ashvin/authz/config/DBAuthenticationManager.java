package com.ashvin.authz.config;

import com.ashvin.platform.domain.User;
import com.ashvin.platform.metrics.provider.MetricsProvider;
import com.ashvin.platform.repo.UserRepo;
import io.opentracing.Tracer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

@Configuration
@Service
public class DBAuthenticationManager implements AuthenticationManager {

    private Logger logger = LoggerFactory.getLogger(DBAuthenticationManager.class);
    private UserRepo userRepo;
    private MetricsProvider metricsProvider;
    private Tracer tracer;
    private PasswordEncoder passwordEncoder;

    @Inject
    public DBAuthenticationManager(DataSource dataSource, MetricsProvider metricsProvider, Tracer tracer, PasswordEncoder passwordEncoder){
        this.passwordEncoder = passwordEncoder;
        this.metricsProvider = metricsProvider;
        this.tracer = tracer;
        this.userRepo = new UserRepo(dataSource, tracer);
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            // TODO: Handle following use cases
            // - Disabled user
            if(authentication instanceof OAuth2Authentication) {
                logger.info("******* OAuth2Authentication");
            }
            metricsProvider.getMeterRegistry().counter("authn_attempts").increment();
            tracer.activeSpan().log("--> Authenticating user: " + authentication.getPrincipal().toString());
            Optional<User> optionalUser = userRepo.findUserByEmail(authentication.getPrincipal().toString());
            if(optionalUser.isEmpty()) {
                throw new BadCredentialsException("Incorrect username or password");
            }
            User dbUser = optionalUser.get();
            if(passwordEncoder.matches(authentication.getCredentials().toString(), dbUser.getPassword())) {
                metricsProvider.getMeterRegistry().counter("authn_success").increment();
                UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(),
                        new ArrayList<>());
                authenticationToken.setDetails(authentication.getDetails());
                return authenticationToken;
            }
            else {
                throw new BadCredentialsException("Incorrect username or password");
            }
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
        return authentication;
    }
}
