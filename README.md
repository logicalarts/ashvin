**Minikube setup**

* Install Minikube and KVM2 Driver
* Start Minikube

    `minikube start --vm-driver=kvm2`
    
* Enable Ingress addon for Minikube

    `minikube addons enable ingress`
    
**CI/CD**
* Install 'infra' helm chart
    
    `cd ${repo}/server/deployment`

    `helm install infra -n ashvin-infra`

* Install skaffold

* Use skaffold to deploy app to minikube

    `cd ${repo}/server`

    `skaffold dev --trigger notify` or `skaffold deploy` 


**Deploying app to Minikube**

* Run Helm chart for ashvin app

    `helm install ashvin -n ashvin-app`

* List Helm releases
    
    `helm list`
    
* Delete a release
    
    `helm delete {release name}`
