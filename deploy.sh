# Dev environment deployment using Skaffold

# One time patch to expose DB outside minikube cluster
# kubectl patch deployment nginx-ingress-controller -n kube-system --patch '{"spec":{"template":{"spec":{"hostNetwork":true}}}}'
# kubectl patch configmap tcp-services -n kube-system --patch '{"data":{"33060":"default/ashvin-db:33060"}}'
# kubectl patch configmap tcp-services -n kube-system --patch '{"data":{"3306":"default/ashvin-db:3306"}}'

# Prepare UI project for creating Docker images
echo "Reusing minikube docker environment"
eval $(minikube docker-env)
echo "Building UI"
cd ui/web || exit
# Install required packages
npm install
# Build UI code
if npm run build; then
  # If UI build is successfull, deploy via Skaffold
	echo "Deploying via Skaffold"
	cd ../..
	skaffold dev --trigger=notify
fi
